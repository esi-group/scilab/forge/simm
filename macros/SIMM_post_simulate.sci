//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function []=SIMM_post_simulate(%cpr, scs_m, needcompile)
disp("SIMM post")
  // find SCOPE bloc for plotting at the end of simulation
  presence_scope=%f;
  list_scope=[];
  display_now=0;
  grid_on=0;
  
  for i = 1:size(scs_m.objs)
    curObj= scs_m.objs(i);
    if (typeof(curObj) == "Block" & curObj.gui == "ISCOPE")
      presence_scope=%t 
      list_scope($+1)=i;
  elseif (typeof(curObj) == "Block" & curObj.gui == "IREP_TEMP") then
      presence_ireptemp=%t 
      display_now=evstr(scs_m.objs(i).graphics.exprs(4))
      grid_on=evstr(scs_m.objs(i).graphics.exprs(3))
    end
  end 
    // adjust scope and add grid
   if presence_scope & ~display_now then
     plot_after_simul(list_scope,scs_m)
   else
      nicescope()     
      if grid_on then
          list_fig=winsid();
          for i=list_fig
              scf(i);
              xgrid;
          end
      end
   end

  

  disp('Fin SIMM post_simulate')

endfunction


function plot_after_simul(list_scope,scs)

    nb_outputs_by_scope=[];
    nb_total_outputs=0;
    nb_scope=size(list_scope,1);
    legendes=cell();
//    grid_on=0;

   for i=1:size(list_scope,1)
       j=list_scope(i);
       obj=scs_m.objs(j);       
       nb_outputs($+1)=evstr(obj.graphics.exprs(1));
       legendes(i).entries=obj.graphics.exprs(3:$);
       nb_total_outputs=nb_total_outputs+nb_outputs($);
   end

    //c_color=[[0.75,0.75,0];[0.25,0.25,0.25];[0,0,1];[0,0.5,0];[1,0,0];[0,0.75,0.75];[0.75,0,0.75]];
    c_color=['yellow','blue','red','black','green','magenta','cyan']
    handle_fig=figure();
    set(handle_fig,"background",8)
    drawlater();
    //extraction des champs stockés
    D=[];
    legend_c=[];
    nb_objs_in_scopeblock=7;
    
    for i=1:nb_scope
        subplot(nb_scope,1,i);
        //legend_c=strsplit(scs.objs(num_scope(i)).graphics.exprs(2)," ");
        legend_c=legendes(i).entries;
        if size(legend_c,1)~=nb_outputs(i) then
            legend_c=_gettext("curv")+string([1:nb_outputs(i)]);
        end
        
        list_obj=scs_m.objs(list_scope(i)).model.rpar.objs;
        no=1;
        for j=1:size(list_obj)
            if (typeof(list_obj(j)) == "Block" & list_obj(j).gui == "TOWS_c") then
                label=list_obj(j).graphics.exprs(2);
                D(i,no)=evstr(label);
                no=no+1;                                    
            end
        end

        for no=1:nb_outputs(i)
            //plot(D(i,no).time,D(i,no).values,'color',[c_color(modulo(no,6)+1,1),c_color(modulo(no,6)+1,2),c_color(modulo(no,6)+1,3)],'thickness',2)
            plot(D(i,no).time,D(i,no).values,'color',c_color(modulo(no,6)+1),'thickness',2)
        end
        ax=gca()
        
        for i=1:size(ax.children,1)
            ax.children(i).children.display_function = "formatITempTip";
        //    ok=datatipInitStruct(ax.children(i).children,"formatfunction","formatITempTip")
        end
        
        h=legend(legend_c);
        set(h,"background",8)
        xgrid

        //title("scope_"+string(i));
    end
    drawnow();
endfunction


