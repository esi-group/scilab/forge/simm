//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2014 - Violeau David
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=IPARAM_VAR(job,arg1,arg2)
  x=[];y=[];typ=[];
  select job
   case 'plot' then
// deprecated
   case 'getinputs' then
// deprecater
   case 'getoutputs' then
// deprecated
   case 'getorigin' then
// deprecated
   case 'set' then
    x=arg1;
    graphics=arg1.graphics;
    exprs=graphics.exprs
    model=arg1.model;

// Boite de dialogue pour indiquer les paramètres à faire varier (3 maximum) et leurs valeurs
    while %t do
      [ok,param1,param1val,param2,param2val,param3,param3val, exprs]=scicos_getvalue('Analyse paramétrique',..
                                                ['Nom du 1er paramètre';'Valeurs du 1er parametre';'Nom du 2nd parametre';'Valeurs du 2nd parametre';...
                                                'Nom du 3eme parametre';'Valeurs du 3eme parametre'], ..
                                                list('vec',-1,'vec',-1,'vec',-1,'vec',-1,'vec',-1,'vec',-1),exprs);
          mess=[];

          if ~ok then
              break;
          end

            // Verification de l'existence des paramètres à faire varier
            // La verification dans le contexte est faite automatiquement dans le scicos_getvalue
            if ~isempty(param1) & isempty(param1val) then
                mess = [mess ;"Il est impératif de rentrer des valeurs pour le premier paramètre"];
				ok = %f;
            end
            if ~isempty(param2) & isempty(param2val) then
                mess = [mess ;"Il est impératif de rentrer des valeurs pour le second paramètre"];
				ok = %f;
            end
             if ~isempty(param3) & isempty(param3val) then
                mess = [mess ;"Il est impératif de rentrer des valeurs pour le troisième paramètre"];
				ok = %f;
            end
            if isempty(param1) & (~isempty(param2) | ~isempty(param3)) then
               mess = [mess ;"Il est impératif de renseigner le paramètre 1 avant les paramètres 2 et 3"];
				ok = %f; 
            end
            if ~isempty(param1) & isempty(param2) & ~isempty(param3) then
               mess = [mess ;"Il est impératif de renseigner le paramètre 2 avant le paramètre 3"];
				ok = %f; 
            end
               
          if ok then
              model.opar=list(exprs(1),param1val,exprs(3),param2val,exprs(5),param3val)
              graphics.exprs=exprs;
              x.model=model;
              x.graphics=graphics;
              break;
          else
              message(mess);
          end

    end


     case 'define' then
      model=scicos_model();
        k=1;
        model.sim=list("IPARAM_VAR",99) // 99 type blocks are ignored by simulator.
        model.blocktype='c';
        model.dep_ut=[%f %f];
        model.in=[];
        model.intyp=[];
        model.out=[];
        model.outtyp=[];
        model.ipar=[1]
        model.rpar=[1;1];
        model.opar=list("k", [1 2 3], "", [], "", [])
        x=standard_define([6 2],model,[],[]);
        x.graphics.in_implicit=[];
        x.graphics.out_implicit=[];
        x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel=Etude Paramétrique"]
        x.graphics.exprs=["k";"[1 2 3]";"";"";"";""]
    end
endfunction
