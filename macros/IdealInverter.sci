// Coselica Toolbox for Xcos
// modif Thierry ROYANT 2014   Diode d'un panneau solaire suite modifs 5 2015
// Copyright (C) 2011 - DIGITEO - Bruno JOFRET
// Copyright (C) 2009, 2010  Dirk Reusch, Kybernetik Dr. Reusch
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

function [x,y,typ]=IdealInverter(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
     case 'set' then
      x=arg1;
      graphics=arg1.graphics;exprs=graphics.exprs;
      model=arg1.model;
      while %t do
          [ok,dephini,exprs]=...
              getvalue(['IdealInverter';__('Onduleur idéal pour moteur synchrone')],...
                       [__('déphasage initial entre le champ électrique et l''angle du rotor nul')],...
                       list('vec',1),exprs);
          if ~ok then break, end
          
          model.equations.parameters(2)=list(dephini)

          graphics.exprs=exprs;
          x.graphics=graphics;x.model=model;
          break
      end
     case 'define' then
      model=scicos_model();
      dephini=0;
      model.sim='SIMM';
      model.blocktype='c';
      model.dep_ut=[%t %f];
      mo=modelica();
      mo.model='SIMM.IdealInverter';
      mo.inputs=['ampl','thetae'];
      mo.outputs=['n1','n2','n3'];
      mo.parameters=list(['dephini'],...
                         list(dephini),...
                         [0]);
      model.equations=mo;
      model.in=ones(size(mo.inputs,'*'),1);
      model.out=ones(size(mo.outputs,'*'),1);
      exprs=string([dephini]);
      gr_i=[];
      x=standard_define([5 5],model,exprs,list(gr_i,0));
      x.graphics.in_implicit=['I','I'];
      x.graphics.in_style=[RealInputStyle(),RealInputStyle()];
      x.graphics.out_implicit=['I','I','I'];
      x.graphics.out_style=[ElecOutputStyle(),ElecOutputStyle(),ElecOutputStyle()];
      in_label=["U",'thetae']
      x.graphics.in_label=in_label;
      out_label=["n1","n2","n3"]
      x.graphics.out_label=out_label;
    end
endfunction
