//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012-2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=BatterieChaude(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'set' then
        x=arg1;
        graphics=x.graphics;
        exprs=graphics.exprs;
        model=x.model;
        while %t do
            [ok,Z,ETA,exprs]=...
              getvalue(['Aeraulique - BatterieChaude';__('Batterie Chaude')],...
                       [__('Coefficient de perte de charges (Pa.h^2.m^(-6))');...
                        __('Rendement (entre 0 et 1)')],...
                       list('vec',1,'vec',1),exprs);

            if ~ok then
                break
            end

            if ok then
                model.equations.parameters(2)=list(Z,ETA)
                graphics.exprs=exprs;
                x.graphics=graphics;x.model=model;
                break
            end
        end
     case 'define' then
      model=scicos_model();
      //parametres par defaut
      Z=10;
      ETA=1;
      model.sim='SIMM';
      model.blocktype='c';
      model.dep_ut=[%t %f];
      mo=modelica();
      mo.parameters=list(['Z','ETA'],...
                         list(Z,ETA),...
                         [0,0]);
      mo.model='Aeraulic.Components.BatterieChaude';
      mo.inputs=['entree','Pt'];
      mo.outputs=['sortie'];
      model.equations=mo;
      model.in=ones(size(mo.inputs,'*'),1);
      model.out=ones(size(mo.outputs,'*'),1);
      exprs=string([Z;ETA]);
      x=standard_define([2 2],model,exprs,list([],0));
      x.graphics.in_implicit=['I','I'];
      x.graphics.in_style=[AeraulicInputStyle(),RealInputStyle()];
      x.graphics.out_implicit=['I'];
      x.graphics.out_style=[AeraulicOutputStyle()];
    end
endfunction
