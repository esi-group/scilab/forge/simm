// This file is released under the 3-clause BSD license. See COPYING-BSD.

function buildmacros()
  macros_path = get_absolute_file_path("buildmacros.sce");
  tbx_build_macros(TOOLBOX_NAME+"_A_", macros_path);
endfunction

function buildblocks()
  macros_path = get_absolute_file_path("buildmacros.sce");
  sources_blocks = [
                    "SourceAir"
                    "SortieAir"
                   ]
  basic_blocks = [
                  "BatterieChaude"  
                    "BatterieFroide" 
    "Humidificateur" // 1 entree, 1 sortie, 1 entree signal
    "ConduiteAeraulic" // 1 entree, 1 sortie, 1 entree signal
    "Ventilateur" // 1 entree, 1 sortie, 1 entree signale
    "Registre" // 1 entree, 1 sortie, 1 entree signale
    "Echangeur" // 2 entree , 2 sorties mixees
    "CaissonMelange" // 2 entree 1 sortie
    "DivergenceAeraulic" //1 entree 2 sortie

                 ]
  parts_blocks = [
  
                 ]
                  
  sensors_blocks=[
                    "PressureSensorAeraulic"
                    "HygrometrySensor"
                     "TempSensorAeraulic"
                    "FlowMassicSensor"] 
  blocks = [sources_blocks
            sensors_blocks
            basic_blocks
            parts_blocks
           ]
           
  tbx_build_blocks(toolbox_dir, blocks, macros_path);
endfunction


buildmacros();
getd(get_absolute_file_path("buildmacros.sce"));
buildblocks();
clear buildmacros; // remove buildmacros on stack
