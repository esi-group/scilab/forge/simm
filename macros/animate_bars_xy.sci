//
// Copyright (C) 2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
  
//function scs_m=animate_bars_xy(scs_m)
// Recherche des bars et du bloc AnimBar2D
//     nb_bars=0;
//     list_bars=[];
//     rep_anim=0
//     for i = 1:size(scs_m.objs)
//        curObj= scs_m.objs(i);
//        if (typeof(curObj) == "Block" & (curObj.gui == "CMPP_FixedTranslation" | curObj.gui == "CMPP_FixedRotation"))
//            nb_bars=nb_bars+1
//            list_bars=[list_bars,i]
//        else if typeof(curObj) == "Block" & curObj.gui == "AnimBar2D"  then
//            rep_anim=i
//        end
//     end

function diagram=animate_bars_xy(nb_bars,parameters)

     //parameters=scs_m.objs(rep_anim).graphics.exprs(1:5)
    
     

     
     //nb_obj_ini=size(scs_m.objs)
     nb_obj_ini=0 //pour test
     diagram=scicos_diagram();
     
     diagram.objs(1)=MUX_f('define')
     diagram.objs(1)
     diagram.objs(2)=MUX_f('define')
     diagram.objs(3)=BARXY('define')
     diagram.objs(4)=SampleCLK('define')
     diagram.objs(5)=ExtractPosBar('define');
     diagram.objs(6)=INIMPL_f('define');
     diagram.objs(6).graphics.exprs=string(1)
     diagram.objs(6).model.ipar=1
     diagram.objs(7)=INIMPL_f('define');
     diagram.objs(7).graphics.exprs=string(2)
     diagram.objs(7).model.ipar=2
     diagram.objs(8)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[5,-1], from=[4, 1,0], to=[3, 1,1])
     diagram.objs(9)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[1, 1,0], to=[3, 1,1])
     diagram.objs(10)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[2, 1,0], to=[3, 1,1])
     diagram.objs(11)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[5, 1,0], to=[1, 1,1])
     diagram.objs(12)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[5, 2,0], to=[2, 1,1])
     diagram.objs(13)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[5, 3,0], to=[1, 2,1])
     diagram.objs(14)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[5, 4,0], to=[2, 2,1])
     diagram.objs(15)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,2], from=[6, 1,0], to=[5, 1,1])
     diagram.objs(16)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,2], from=[7, 1,0], to=[5, 2,1])
     
     
     
     //ajout de deux Mux, d'un BarXY et d'une clock
     diagram.objs(1)=MUX_f('define')
     diagram.objs(1).graphics.exprs=string(2*nb_bars)
     diagram.objs(1).model.ipar=2*nb_bars
     diagram.objs(1).model.in=[-1:-1:-2*nb_bars]
     diagram.objs(1).model.in2=1*ones(2*nb_bars,1)
     diagram.objs(1).model.intyp=1*ones(2*nb_bars,1)
     input2=string(ones(nb_bars*2,1))
     diagram.objs(1).graphics.in_implicit=strsubst(input2,'1','E');
     
     diagram.objs(2)=MUX_f('define')
     diagram.objs(2).graphics.exprs=string(2*nb_bars)
     diagram.objs(2).model.ipar=2*nb_bars
     diagram.objs(2).model.in=[-1:-1:-2*nb_bars]
     diagram.objs(2).model.in2=1*ones(2*nb_bars,1)
     diagram.objs(2).model.intyp=1*ones(2*nb_bars,1)
     diagram.objs(2).graphics.in_implicit=strsubst(input2,'1','E');
     
     diagram.objs(3)=BARXY('define')
     diagram.objs(3).graphics.exprs=parameters(1:5)
     diagram.objs(3).model.rpar=evstr(parameters(1:4))
     diagram.objs(3).model.ipar=evstr(parameters(5))
     diagram.objs(3).model.in2=1*ones(2,1)
     diagram.objs(3).model.intyp=1*ones(2,1)
     
     diagram.objs(4)=SampleCLK('define')
     diagram.objs(4).graphics.exprs(1)=parameters(6)
     diagram.objs(4).model.rpar=evstr([parameters(6);"0"])
     
     //ajout de nb_bars blocs ExtractPosBar
     for i= 1:nb_bars
         diagram.objs(4+i)=ExtractPosBar('define');
     end
     
     //pour test : ajout des blocs d'entrees IN_f
     //sinon ajout des blocs SPLIT_f
     for i=1:nb_bars
         diagram.objs(4+nb_bars+2*i-1)=INIMPL_f('define')
         diagram.objs(4+nb_bars+2*i-1).graphics.exprs=string(2*i-1)
         diagram.objs(4+nb_bars+2*i-1).model.ipar=2*i-1
         diagram.objs(4+nb_bars+2*i)=INIMPL_f('define')
         diagram.objs(4+nb_bars+2*i).graphics.exprs=string(2*i)
         diagram.objs(4+nb_bars+2*i).model.ipar=2*i
     end
          
     //gestion des liens internes
     nb_links_ini=nb_obj_ini+3*nb_bars+4
     //liens fixes (clock_barxy, mux-barxy)
     diagram.objs(1).graphics.pout=nb_links_ini+2
     diagram.objs(2).graphics.pout=nb_links_ini+3
     diagram.objs(3).graphics.pin=[nb_links_ini+2;nb_links_ini+3]
     diagram.objs(3).graphics.pein=nb_links_ini+1
     diagram.objs(4).graphics.peout=nb_links_ini+1
     diagram.objs(4+3*nb_bars+1)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[5,-1], from=[nb_obj_ini+4, 1,0], to=[nb_obj_ini+3, 1,1])
     diagram.objs(4+3*nb_bars+2)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[nb_obj_ini+1, 1,0], to=[nb_obj_ini+3, 1,1])
     diagram.objs(4+3*nb_bars+3)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[nb_obj_ini+2, 1,0], to=[nb_obj_ini+3, 2,1])
     
     //liens extractbar-mux
     for i=1:nb_bars   
         //nouveaux liens
        diagram.objs(7+3*nb_bars+4*i-3)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[nb_obj_ini+4+i, 1,0], to=[nb_obj_ini+1, 2*i-1,1])
        diagram.objs(7+3*nb_bars+4*i-2)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[nb_obj_ini+4+i, 2,0], to=[nb_obj_ini+2, 2*i-1,1])
        diagram.objs(7+3*nb_bars+4*i-1)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[nb_obj_ini+4+i, 3,0], to=[nb_obj_ini+1, 2*i,1]) 
        diagram.objs(7+3*nb_bars+4*i)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[nb_obj_ini+4+i, 4,0], to=[nb_obj_ini+2, 2*i,1]) 
        //dans extractbar
        diagram.objs(4+i).graphics.pout=(nb_links_ini+3+4*i)+[-3;-2;-1;0]
     end
     //dans mux
     diagram.objs(1).graphics.pin=nb_links_ini+3+[1:2:4*nb_bars]'
     diagram.objs(2).graphics.pin=nb_links_ini+3+[2:2:4*nb_bars]'
     
     //liens extractbar-inputs
     for i=1:nb_bars
         //nouveaux liens
        diagram.objs(7+7*nb_bars+2*i-1)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,2], from=[nb_obj_ini+4+nb_bars+2*i-1, 1,0], to=[nb_obj_ini+4+i, 1,1])
        diagram.objs(7+7*nb_bars+2*i)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,2], from=[nb_obj_ini+4+nb_bars+2*i, 1,0], to=[nb_obj_ini+4+i, 2,1])
        //dans inputs
        diagram.objs(4+nb_bars+2*i-1).graphics.pout=nb_links_ini+3+4*nb_bars+2*i-1
        diagram.objs(4+nb_bars+2*i).graphics.pout=nb_links_ini+3+4*nb_bars+2*i
        //dans extractbar
        diagram.objs(4+i).graphics.pin=nb_links_ini+3+4*nb_bars+2*i+[-1;0]
     end
    
  //scs_m=resume(scs_m);

endfunction
