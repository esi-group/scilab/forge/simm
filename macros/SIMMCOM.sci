//  Scicos
//
//  Copyright (C) INRIA - METALAU Project <scicos@inria.fr>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//
// See the file ../license.txt
//

function [ok,tt]=SIMMCOM(funam,tt,vinp,vout,vparam,vparamv,vpprop,intype,outtype)
    //
    [dirF,nameF,extF]=fileparts(funam);

    //the new head
    class_txt_new=build_classhead_simm(funam,vinp,vout,vparam,vparamv,vpprop,intype,outtype)

    if (tt==[]) then
        tete4= ["";" //    Vous pouvez definir après ce commentaire d''autres variables internes ou parametres";" //Exemple :  Real x(start=1), y(start=2); Parameter Real a=1;";""]
        tete5="equation";

        tete6=["      // Renseigner ici votre fonction"];
        tete7="      // Les noms des variables de flux et potentiel associées à chaque connecteur sont définies dans la documentation.";
        tete8="      // Exemple : der(flange.phi) pour la dérivée de la position angulaire";
        tete8bis="   ";
        tete9="end "+nameF+";";
        textmp=[class_txt_new;tete4;tete5;tete6;tete7;tete8;tete8bis;tete9];
    else
        modif=%f;
        for i=1:size(tt,"*")
            if strindex(stripblanks(tt(i)),...
                "////Ne pas modifier avant cette ligne ////")<>[] then
                textmp=[class_txt_new;tt(i+1:$)]
                modif=%t
                break
            end
        end
        if ~modif then textmp=tt, end;
    end

    editblk=%f
    //## set param of scstxtedit
    ptxtedit = scicos_txtedit(clos = 0,...
    typ  = "ModelicaClass",...
    head = ["Definition d''une fonction Modelica";
    "A cet endroit"+...
    " editer le corps de la fonction"])

    while %t

        if (extF=="" | (extF==".mo" & fileinfo(funam)==[])) then
            editblk=%t;
            [txt,Quit] = scstxtedit(textmp,ptxtedit);
        elseif (extF==".mo" & fileinfo(funam)<>[]) then
            txt=tt;
        end


        if ptxtedit.clos==1 then
            break;
        end

        if txt<>[] then
            //## TODO : compilation
            //## printf("Compil !!");
            ok=%t;

            //** saving in the filename
            if ok then
                tarpath=pathconvert(TMPDIR+"/Modelica/",%f,%t);

                if (extF=="")  then
                    funam=tarpath+nameF+".mo";
                    mputl(txt,funam);
                elseif fileinfo(funam)==[] then
                    mputl(txt,funam);
                end
                ptxtedit.clos = 1;
                tt   = txt;
            end
            textmp    = txt;
        end

        if editblk then
            if Quit==1 then
                ok=%f;
                break;
            end
        elseif txt==[] then
            ok=%f; // cancel bouton
            break
        end
    end
endfunction

//build_classhead : build the head of the modelica function
function class_txt=build_classhead_simm(funam,vinp,vout,vparam,vparamv,vpprop,intype,outtype)

    [dirF,nameF,extF]=fileparts(funam);

    ni=size(vinp,"r");   //** number of inputs
    no=size(vout,"r");   //** number of outputs
    np=size(vparam,"r"); //** number of params

    tete1=["class "+nameF]

    //** parameters head
    if np<>0 then
        tete1b= "      //parameters";
        for i=1:np
            //** param
            if vpprop(i)==0 then
                head="      parameter Real "
                if size(vparamv(i),"*")==1 then
                    head=head+msprintf("%s = %e;", vparam(i), vparamv(i));
                else
                    head=head+vparam(i)+"["+string(size(vparamv(i),"*"))+"]={";
                    for j=1:size(vparamv(i),"*")
                        head=head+msprintf("%e", vparamv(i)(j));
                        if j<>size(vparamv(i),"*") then
                            head=head+","
                        end
                    end
                    head=head+"};"
                end
                //** state
            elseif vpprop(i)==1 then
                head="      Real           "
                if size(vparamv(i),"*")==1 then
                    head=head+msprintf("%s (start=%e);", vparam(i), vparamv(i));
                else
                    head=head+vparam(i)+"["+string(size(vparamv(i),"*"))+"](start={";
                    for j=1:size(vparamv(i),"*")
                        head=head+msprintf("%e", vparamv(i)(j));
                        if j<>size(vparamv(i),"*") then
                            head=head+","
                        end
                    end
                    head=head+"});"
                end
                //** fixed state
            elseif vpprop(i)==2 then
                head="      Real           "
                if size(vparamv(i),"*")==1 then
                    head=head+msprintf("%s (fixed=true,start=%e);", vparam(i), vparamv(i));
                else
                    head=head+vparam(i)+"["+string(size(vparamv(i),"*"))+"](start={";
                    P_fix="fixed={"
                    for j=1:size(vparamv(i),"*")
                        head=head+msprintf("%e", vparamv(i)(j));
                        P_fix=P_fix+"true"
                        if j<>size(vparamv(i),"*") then
                            head=head+","
                            P_fix=P_fix+","
                        end
                    end
                    head=head+"},"+P_fix+"});"
                end
            end
            tete1b=[tete1b
            head]
        end
    else
        tete1b=[];
    end
    
 list_var=list([".v potentiel en V",".i courant en A"],...
 [".p pression en Pa",".q débit en m3/s"],...
 [".p pression en Pa",".q débit m3/s",".h taux humidité en %"],...
 [".phi angle en rad",".tau couple en Nm"],...
 [".f force en N",".s position en m"],...
 [".f force 2D en N",".t couple en Nm",".r_0 position absolue en m",".phi angle en rad"],...
 [".T température en Kelvin",".Q_flow flux thermique en W"],...
 [".signal signal sans unité"])
       
    //** inputs head
    if ni<>0 then
        list_type_var_plus=["Modelica.Electrical.Analog.Interfaces.PositivePin",...
        "SIMM.Hydraulic.Connectors.Interfaces.PortA",....
        "SIMM.Aeraulic.Interfaces.PortA",....
        "Modelica.Mechanics.Rotational.Interfaces.Flange_a",....
        "Modelica.Mechanics.Translational.Interfaces.Flange_a",....
        "Coselica.Mechanics.Planar.Interfaces.Frame_a",....
        "Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a",....
        "Modelica.Blocks.Interfaces.RealInput"]
        list_type_var_moins=["Modelica.Electrical.Analog.Interfaces.NegativePin",...
        "SIMM.Hydraulic.Connectors.Interfaces.PortB",....
        "SIMM.Aeraulic.Interfaces.PortB",....
        "Modelica.Mechanics.Rotational.Interfaces.Flange_b",....
        "Modelica.Mechanics.Translational.Interfaces.Flange_b",....
        "Coselica.Mechanics.Planar.Interfaces.Frame_b",....
        "Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_b",....
        "Modelica.Blocks.Interfaces.RealOutput"]
        list_styles=["El","Hy","Ae","Ro","Tr","Pl","Th","Si"]
        tete_input=["      //input variables"]
        
        
        for i=1:ni
            tete2= "      ";
            type_var="Real "
            if intype(i)=="E" | intype(i)=="I" then
                type_var="Real ";
                commentaire="      //Utilisez directement la variable "+vinp(i)
            else 
                repp=find(list_styles+"+"==intype(i))
                repm=find(list_styles+"-"==intype(i))
                if ~isempty(repp) then
                    type_var=list_type_var_plus(repp)
                    type_comment=list_var(repp)
                elseif ~isempty(repm) then
                    type_var=list_type_var_moins(repm)
                    type_comment=list_var(repm)
                 end
                commsuppl=""
                for kk=1:size(type_comment,"*")
                    commsuppl= commsuppl+vinp(i)+string(type_comment(kk))+", "
                end
                commentaire="      //les variables disponibles sont "+commsuppl
            end

            tete2=tete2+type_var+" "+vinp(i)
            tete2=tete2+";";
            tete_input($+1)=tete2
            
            tete_input($+1)=commentaire
        end
    else
        tete_input=[];
    end

    //** outputs head
    if no<>0 then
        list_type_var_plus=["Modelica.Electrical.Analog.Interfaces.PositivePin",...
        "SIMM.Hydraulic.Connectors.Interfaces.PortA",....
        "SIMM.Aeraulic.Interfaces.PortA",....
        "Modelica.Mechanics.Rotational.Interfaces.Flange_a",....
        "Modelica.Mechanics.Translational.Interfaces.Flange_a",....
        "Coselica.Mechanics.Planar.Interfaces.Frame_a",....
        "Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_a",....
        "Modelica.Blocks.Interfaces.RealInput"]
        list_type_var_moins=["Modelica.Electrical.Analog.Interfaces.NegativePin",...
        "SIMM.Hydraulic.Connectors.Interfaces.PortB",....
        "SIMM.Aeraulic.Interfaces.PortB",....
        "Modelica.Mechanics.Rotational.Interfaces.Flange_b",....
        "Modelica.Mechanics.Translational.Interfaces.Flange_b",....
        "Coselica.Mechanics.Planar.Interfaces.Frame_b",....
        "Modelica.Thermal.HeatTransfer.Interfaces.HeatPort_b",....
        "Modelica.Blocks.Interfaces.RealOutput"]
        list_styles=["El","Hy","Ae","Ro","Tr","Pl","Th","Si"]
        tete_output=["      //output variables"]
        
        for i=1:no
            tete2= "      ";
            type_var="Real "
            if outtype(i)=="E" | outtype(i)=="I" then
                type_var="Real ";
                commentaire="      //Utilisez directement la variable "+vout(i)
            else 
                repp=find(list_styles+"+"==outtype(i))
                repm=find(list_styles+"-"==outtype(i))
                if ~isempty(repp) then
                    type_var=list_type_var_plus(repp)
                    type_comment=list_var(repp)
                elseif ~isempty(repm) then
                    type_var=list_type_var_moins(repm)
                    type_comment=list_var(repm)
                 end
                 commsuppl=""
                 for kk=1:size(type_comment,"*")
                    commsuppl= commsuppl+vout(i)+string(type_comment(kk))+", "
                 end
                 commentaire="      //les variables disponibles sont "+commsuppl
            end
            
            tete2=tete2+type_var+" "+vout(i)
            tete2=tete2+";";
            tete_output($+1)=tete2
            tete_output($+1)=commentaire
        end
    else
        tete_output=[];
    end

    tete4="  ////Ne pas modifier avant cette ligne ////"
    //-----------------------------------------

    class_txt=[tete1;
    "  ////Generation automatique ////";
    tete1b;tete_input;tete_output;tete4]
endfunction
