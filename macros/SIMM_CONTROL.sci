//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=SIMM_CONTROL(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'set' then
        x=arg1;
        graphics=arg1.graphics;
        exprs=graphics.exprs
        model=arg1.model;

        disp(job)
//        //Choix du type de diagramme
//        while (%t)  do
//            [ok,tf,num_pts,grid_on,display_now,exprs]=scicos_getvalue(__('Scope parameters'),..
//                                                        [gettext('Durée de la simulation');gettext('Nombre de points');gettext("Grille affichée (1 oui, 0 non)");gettext("Affichage des courbes pendant la simulation (1 oui, 0 non)")], ..
//                                                        list('vec',1,'vec',1,'vec',1,'vec', 1), ..
//                                                        exprs)
//          mess=[];
//
//          if ~ok then
//// Cancel
//              break;
//          end
//
//          if num_pts <= 0
//              mess=[mess ;_("Le nombre de points doit être positif")]
//              ok = %f
//          end
//
//          if grid_on ~=1 & grid_on~=0
//              mess=[mess ;_("Taper 1 ou 0 pour afficher la grille ou non")]
//              ok = %f
//          end
//
//        if display_now ~=1 & display_now~=0
//              mess=[mess ;_("Taper 1 ou 0 pour afficher les courbes pendant la simulation ou à la fin uniquement")]
//              ok = %f
//          end
//
//
//          if ok then
//// Everything's ok
//                model.rpar(1)=num_pts;
//                model.rpar(2)=tf;
//              graphics.exprs =  exprs;
//              x.model=model;
//              x.graphics = graphics;
//              break
//          else
//              message(mess);
//          end
//
 //     end
     case 'define' then
        model=scicos_model()
        model.sim=list("SIMM_CONTROL",99)
        model.blocktype="c"
        model.firing=[]
        model.dep_ut=[%f %f]

        gr_i=[]
        x=standard_define([3 2],model,[],gr_i)
        label = "CONTROL"
      x.graphics.style = ["blockWithLabel;displayedLabel="+label];
    end

endfunction
