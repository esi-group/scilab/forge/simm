//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2014 Violeau David
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function continueSimulation=IParam_Temp_pre_simulate(scs_m, needcompile)

    //On crée un nouveau fichier scs pour ne pas écraser le premier
    scs=scs_m
    objects = scs_m.objs;

    nombre_blocs=0;    //Nombre de blocs dans le diagramme
    nombre_liens=0;    //Nombre de liens dans le diagramme

    param1="";
    param1val=[];
    param2="";
    param2val=[];
    param3="";
    param3val=[];

    todemux = %f;

    continueSimulation = %f;

    //Récupère le nombre de blocs dans le modèle
    for i=1:size(scs.objs)
        if typeof(scs.objs(i))=='Block' then
            nombre_blocs=nombre_blocs+1;
        end
    end
    nombre_liens=size(scs.objs)-nombre_blocs;  //calcul du nombre de liens

    nb_scopes=0 //Nombre de scopes trouvés
    nom_scope=list();
    num_scope=[];    //numéro du bloc correspondant à chaque SCOPE
    nb_curves_by_scope=[]; //nombre de courbes à tracer sur un même SCOPE
    nb_total_curves=0;    //nombre total de courbes à tracer
    legendes=cell();
    // On récupere le numéro du bloc PARAM_VAR puis les caractéristiques correspondantes
    // On récupère le bloc REP_TEMP pour les paramètres temporels
    num_param=0
    display_now=0
    presence_scope=%f;
    list_scope=[];
    for i = 1:size(scs.objs)
        curObj= scs.objs(i);
        if (typeof(curObj) == "Block" & curObj.gui == "ISCOPE")
            presence_scope=%t 
            list_scope($+1)=i;
        elseif (typeof(curObj) == "Block" & curObj.gui == "IPARAM_VAR")
            if ~isempty(curObj.model.opar(1)) then
                param1=curObj.model.opar(1)
                param1val=curObj.model.opar(2)
                num_param=num_param+1
            end
            if ~isempty(curObj.model.opar(3)) then
                param2=curObj.model.opar(3)
                param2val=curObj.model.opar(4)
                num_param=2
            end
            if ~isempty(curObj.model.opar(5)) then
                param3=curObj.model.opar(5)
                param3val=curObj.model.opar(6)
                num_param=3
            end
        elseif (typeof(curObj) == "Block" & curObj.gui == "IREP_TEMP")
            nb_pts=curObj.model.rpar(1);
            temps=curObj.model.rpar(2);
            grid_on=evstr(curObj.graphics.exprs(3));
            scs.props.tf=temps;
            rep_temp=1;
            display_now=evstr(curObj.graphics.exprs(4));
            if display_now==1 then
                message("L''affichae des courbes en cours de simulation n''est pour l''instant pas possible en étude paramétrique")
                return
            end
        end
    end

    // On liste le nombre de blocs SCOPE et on modifie les noms des variables des SCOPE ainsi que les paramètres
    for i = 1:size(scs.objs)
        if (typeof(scs.objs(i)) == "Block" & scs.objs(i).gui == "ISCOPE" )
            nb_scopes=nb_scopes+1;
            num_scope(nb_scopes)=i;
            nb_curves_by_scope(nb_scopes)=evstr(scs.objs(i).graphics.exprs(1));
            sample_time=temps/nb_pts;
            //recherche parmi les blocs la clock et le tows
            list_obj=scs.objs(i).model.rpar.objs;
            no=1;
            for j=1:size(list_obj)
                if (typeof(list_obj(j)) == "Block" & list_obj(j).gui == "TOWS_c") then //on affecte un nom pour le stockage dans scilab
                    scs.objs(i).model.rpar.objs(j).graphics.exprs=[string(nb_pts);"o"+string(no+nb_total_curves);"0"];
                    //disp(scs.objs(i).model.rpar.objs(j).graphics.exprs)
                    scs.objs(i).model.rpar.objs(j).model.ipar=[nb_pts;2;24;no+nb_total_curves];
                    no=no+1;
                elseif (typeof(list_obj(j)) == "Block" & list_obj(j).gui == "SampleCLK") then //on modifie le pas de temps
                    scs.objs(i).model.rpar.objs(j).graphics.exprs=[string(sample_time);"0"];
                    scs.objs(i).model.rpar.objs(j).model.rpar=[sample_time;0];  
                end
            end

            legendes(nb_scopes).entries=scs.objs(i).graphics.exprs(2:$);
            nb_total_curves=nb_total_curves+nb_curves_by_scope(nb_scopes);
        end
    end

    //    //recherche du nombre de paramètres variables
    //    num_param=0;
    //    if ~isempty(param1) then num_param=num_param+1;
    //    end
    //    if ~isempty(param2) then num_param=num_param+1;
    //    end
    //    if ~isempty(param3) then num_param=num_param+1;
    //    end
    //


    //variables pour stockage des résultats
    D=[]
    cont=[]
    z=[]


    //    for z=1:nombre_scope
    //        handle_fig(z)=figure();
    //    end
    //    legend_c=[]

    //toutes les courbes sont réunies sur la même figure, une sous-figure par SCOPE, sur chaque sous-figure, autant de courbes que de simulations et d'entrées d'un bloc scope
    handle_fig=figure();
    set(handle_fig,"background",8)
    legend_by_simul=[]
    drawlater()
    //Lancement d'une barre de progression
    x=0
    winId=waitbar('Simulation en cours...')

    //c_color=[[0.75,0.75,0];[0.25,0.25,0.25];[0,0,1];[0,0.5,0];[1,0,0];[0,0.75,0.75];[0.75,0,0.75]];
    plot_properties.c_color=['y','b','r','k','g','m','c'];
    plot_properties.m_type=['-','--',':','-.'];
    plot_properties.legendes=legendes;
    plot_properties.nb_scopes=nb_scopes;
    plot_properties.nb_curves_by_scope=nb_curves_by_scope;
    //Lancement des simulations
    Nsimu=0  //Numero de la simu
    legend_by_subplot=[];
    scopes_legend=cell(nb_scopes);

    select num_param
    case 1 //1 parametre

        for i=1:length(param1val)
            //mise à jour de la barre de progression
            x=(i-1)/length(param1val)
            waitbar(x,winId)
            //modification du contexte et simulation
            context=scs.props.context;
            cIndex = grep(context, "/"+string(param1)+"[ ]*=/", "r");
            context(cIndex)=string(param1)+"="+string(param1val(i))
            scs.props.context = context;

            cpr=xcos_simulate_simm(scs,4)

            //stockage du nom de la simulation avec la valeur du parametre
            Nsimu=Nsimu+1;

            legend_by_simul(Nsimu)=string(param1)+'='+string(param1val(i))

            iplot_on_scope(scopes_legend,plot_properties,legend_by_simul,scs)

        end     

        delete(winId);

    case 2 //2 parametres

        for i=1:length(param1val)
            context=scs.props.context;
            cIndex = grep(context, "/"+string(param1)+"[ ]*=/", "r");
            context(cIndex)=string(param1)+"="+string(param1val(i))
            for j=1:length(param2val)
                x=((i-1)*length(param2val)+(j-1))/(length(param1val)*length(param2val));
                waitbar(x,winId);
                //modification du contexte et simulation
                cIndex = grep(context, "/"+string(param2)+"[ ]*=/", "r");
                context(cIndex)=string(param2)+"="+string(param2val(j))
                scs.props.context = context;
    
                cpr=xcos_simulate_simm(scs,4)
                Nsimu=Nsimu+1;
                //stockage du nom de la simulation avec la valeur du parametre
                legend_by_simul(Nsimu)=string(param1)+'='+string(param1val(i))+', '+string(param2)+'='+string(param2val(j));
                iplot_on_scope(scopes_legend,plot_properties,legend_by_simul,scs)

            end
        end  

        delete(winId);

    case 3 // trois parametres variables
        for i=1:length(param1val)
            context=scs.props.context;
            cIndex = grep(context, "/"+string(param1)+"[ ]*=/", "r");
            context(cIndex)=string(param1)+"="+string(param1val(i))
            for j=1:length(param2val)
                cIndex = grep(context, "/"+string(param2)+"[ ]*=/", "r");
                context(cIndex)=string(param2)+"="+string(param2val(j))
                for k=1:length(param3val)
                    x=((i-1)*length(param2val)*length(param3val)+(j-1)*length(param3val)+(k-1))/(length(param1val)*length(param2val)*length(param3val));
                    waitbar(x,winId);
                    //modification du contexte et simulation
                    cIndex = grep(context, "/"+string(param2)+"[ ]*=/", "r");
                    context(cIndex)=string(param2)+"="+string(param2val(j))
                    scs.props.context = context;
        
                    cpr=xcos_simulate_simm(scs,4)
                    Nsimu=Nsimu+1

                    legend_by_simul(Nsimu)=string(param1)+'='+string(param1val(i))+', '+string(param2)+'='+string(param2val(j))+', '+string(param3)+'='+string(param3val(k));

                    iplot_on_scope(scopes_legend,plot_properties,legend_by_simul,scs)

                end
            end
        end
        delete(winId);
    end

    set(handle_fig,"background",8)
    //h=legend(scopes_legend.entries);
    h.background=-2;
    if grid_on==1 then
        xgrid
    end       

    //nicescope();
    drawnow()
endfunction

function iplot_on_scope(scopes_legend,plot_properties,legend_by_simul,scs)
    c_color=plot_properties.c_color;
    m_type=plot_properties.m_type;
    legendes=plot_properties.legendes;
    nb_scopes=plot_properties.nb_scopes;
    nb_curves_by_scope=plot_properties.nb_curves_by_scope;   
    legend_by_subplot=[];
    //scopes_legend=cell(nb_scopes);

    for l=1:nb_scopes
        subplot(nb_scopes,1,l);
        legend_by_subplot=legendes(l).entries(2:size(legendes(l).entries,1));
        set(handle_fig,"background",8)

        if size(legend_by_subplot,1)~=nb_curves_by_scope(l) then
            legend_by_subplot="courbe"+string([1:nb_curves_by_scope(l)]);
        end
        scopes_legend(l).entries=[scopes_legend(l).entries legend_by_simul(Nsimu)+" "+legend_by_subplot];

        list_obj=scs.objs(num_scope(l)).model.rpar.objs;
        no=1;
        for m=1:size(list_obj)
            if (typeof(list_obj(m)) == "Block" & list_obj(m).gui == "TOWS_c") then
                label=list_obj(m).graphics.exprs(2);
                D=evstr(label);
                //disp(D)
                plot(D.time,D.values,c_color(modulo(Nsimu,6)+1)+m_type(modulo(no,4)),'thickness',2)
                no=no+1;    
                e=gce()
                //ok=datatipInitStruct(e.children($),"formatfunction","formatTempTip")                       
            end
        end         

        //        for i=1:size(ax.children,1)
        //                  disp(ax.children(i).children($))
        //                  if ax.children(i).children.type ~='legend' then
        //                  ok=datatipInitStruct(ax.children(i).children($),"formatfunction","formatTempTip")
        //                  end
        //              end  

        //if l==nb_scopes
        set(handle_fig,"background",8)
        
        h=legend(scopes_legend(l).entries);
        h.legend_location="upper_caption"
        h.background=-2;
        h.font_size=1;
        if grid_on==1 then
            xgrid
        end
        //end
    end     
    scopes_legend=resume(scopes_legend)
endfunction

function str=formatTempTip(curve,pt,index)
    //this function is called by the datatips mechanism to format the tip
    //string for the magnitude bode curves
    str=msprintf("%.4g"+_("s")+"\n%.4g", pt(1),pt(2))
endfunction

