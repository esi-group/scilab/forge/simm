//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012-2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=IdealCurSourcesStepper(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
     case 'set' then
      x=arg1;
     case 'define' then
      model=scicos_model();
      model.sim='SIMM';
      model.blocktype='c';
      model.dep_ut=[%t %f];
      mo=modelica();
      mo.model='SIMM.IdealCurrentSourcesStepper';
      mo.inputs=['uA','uB'];
      mo.outputs=['pA','nA','pB','nB'];
      model.equations=mo;
      model.in=ones(size(mo.inputs,'*'),1);
      model.out=ones(size(mo.outputs,'*'),1);
      exprs=[];
      x=standard_define([5 5],model,exprs,list([],0));
      x.graphics.in_implicit=['I','I'];
      x.graphics.in_style=[RealInputStyle(),RealInputStyle()];
      x.graphics.out_implicit=['I','I','I','I'];
      x.graphics.out_style=[ElecInputStyle(),ElecOutputStyle(),ElecInputStyle(),ElecOutputStyle()];
                out_label=["pA";"nA";"pB";"nB"]
      x.graphics.out_label=out_label;
                      in_label=["A";"B"]
      x.graphics.in_label=in_label;
    end
endfunction
