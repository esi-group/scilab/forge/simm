//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012-2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
function [x,y,typ]=ExtractPosBar(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'set' then
	   x=arg1
    case 'define' then  
        diagram=scicos_diagram();

        diagram.objs(1)=INIMPL_f('define');
        diagram.objs(1).graphics.exprs="1"
        diagram.objs(1).graphics.pout=15
        diagram.objs(1).model.ipar=1
        diagram.objs(2)=INIMPL_f('define');
        diagram.objs(2).graphics.exprs="2"
        diagram.objs(2).model.ipar=2
        diagram.objs(2).graphics.pout=16
        diagram.objs(3)=CMPS_AbsPosition('define');
        diagram.objs(3).graphics.pin=15
        diagram.objs(3).graphics.pout=17
        diagram.objs(4)=CMPS_AbsPosition('define');
        diagram.objs(4).graphics.pin=16
        diagram.objs(4).graphics.pout=18
        diagram.objs(5)=CBR_DeMultiplex2('define');
        diagram.objs(5).graphics.pin=17
        diagram.objs(5).graphics.pout=[19;20]
        diagram.objs(6)=CBR_DeMultiplex2('define');
        diagram.objs(6).graphics.pin=18
        diagram.objs(6).graphics.pout=[21;22]
        diagram.objs(7)=CBI_RealOutput('define');
        diagram.objs(7).graphics.pin=19
        diagram.objs(7).graphics.pout=26        
        diagram.objs(8)=CBI_RealOutput('define');
        diagram.objs(8).graphics.pin=20
        diagram.objs(8).graphics.pout=25  
        diagram.objs(9)=CBI_RealOutput('define');
        diagram.objs(9).graphics.pin=21
        diagram.objs(9).graphics.pout=24  
        diagram.objs(10)=CBI_RealOutput('define');
        diagram.objs(10).graphics.pin=22
        diagram.objs(10).graphics.pout=23  
        diagram.objs(11)=OUT_f('define');
        diagram.objs(11).graphics.exprs="1"
        diagram.objs(11).graphics.pin=26
        diagram.objs(11).model.ipar=1
        diagram.objs(12)=OUT_f('define');
        diagram.objs(12).graphics.exprs="2"
        diagram.objs(12).graphics.pin=25
        diagram.objs(12).model.ipar=2
        diagram.objs(13)=OUT_f('define');
        diagram.objs(13).graphics.exprs="3"
        diagram.objs(13).graphics.pin=24
        diagram.objs(13).model.ipar=3
        diagram.objs(14)=OUT_f('define');
        diagram.objs(14).graphics.exprs="4"
        diagram.objs(14).graphics.pin=23
        diagram.objs(14).model.ipar=4
        //links
        diagram.objs(15)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,2], from=[1, 1,0], to=[3, 1,1])
        diagram.objs(16)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,2], from=[2, 1,0], to=[4, 1,1])
        diagram.objs(17)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,2], from=[3, 1,0], to=[5, 1,1])
        diagram.objs(18)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,2], from=[4, 1,0], to=[6, 1,1])
        diagram.objs(19)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,2], from=[5, 1,0], to=[7, 1,1])
        diagram.objs(20)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,2], from=[5, 2,0], to=[8, 1,1])
        diagram.objs(21)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,2], from=[6, 1,0], to=[9, 1,1])
        diagram.objs(22)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,2], from=[6, 2,0], to=[10, 1,1])
        diagram.objs(23)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[10, 1,0], to=[14, 1,1])
        diagram.objs(24)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[9, 1,0], to=[13, 1,1])
        diagram.objs(25)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[8, 1,0], to=[12, 1,1])
        diagram.objs(26)=scicos_link(xx=[0 ; 0],yy=[0 ; 0], ct=[1,1], from=[7, 1,0], to=[11, 1,1])
        
        
        model=scicos_model();
        model.sim='csuper'
        model.blocktype='h';
        model.dep_ut=[%f %f];
        model.rpar=diagram
        model.out=[-1;-1;-1;-1]
        model.out2=[-2;-2;-2;-2]
        model.outtyp=[-1;-1;-1;-1]
        model.in=[-1;-1]
        model.in2=[-2;-2]
        model.intyp=[-1;-1]

        x=standard_define([2 2],model,[],[]);
        x.graphics.in_implicit=['I';'I'];
        x.graphics.out_implicit=['E';'E';'E';'E'];
        x.graphics.in_style=[PlanInputStyle(),PlanOutputStyle()];
        x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel=ExtractPosBar"]
        x.graphics.exprs=[];
    end
endfunction
