//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
  
function scs_m=SIMM_control_fct(scs_m)

  list_super=[]
  for i = 1:size(scs_m.objs)
    curObj= scs_m.objs(i);
    
    if (typeof(curObj) == "Block" & curObj.gui == "SUPER_f")
      list_super($+1)=i;
      //liste des ports d'entree et de sortie implicites
      ind_inimplicit=find(curObj.graphics.in_implicit=="I")
      ind_outimplicit=find(curObj.graphics.out_implicit=="I")
        for j = 1:size(curObj.model.rpar.objs)
            subObj=curObj.model.rpar.objs(j)
            if (typeof(subObj) == "Block" & subObj.gui == "OUTIMPL_f")
                //extraction du lien et du numero du bloc dans la liste des ports
                pin=subObj.graphics.pin
                num=evstr(subObj.graphics.exprs)
                //recherche du bloc relie a partir du lien
                pinlink=curObj.model.rpar.objs(pin)
                numobj=pinlink.from(1)
                posobj=pinlink.from(2)
                out_style=curObj.model.rpar.objs(numobj).graphics.out_style(posobj)
                //assignation du style au superbloc
                ind=find(ind_outimplicit==num)
                scs_m.objs(i).graphics.out_style(num)=out_style
            end
                
        end
        
        
      
    end
  end  
  scs_m=resume(scs_m);

endfunction
