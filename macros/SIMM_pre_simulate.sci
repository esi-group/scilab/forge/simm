//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011-2011 - DIGITEO - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
  
function scs_m=SIMM_pre_simulate(scs_m, needcompile)

 
  // find SCOPE bloc for plotting at the end of simulation
 
  
  presence_scope=%f;
  list_scope=[];
  presence_ireptemp=%f;
  list_ireptemp=[];
  display_now=0;
  continueSimulation = %t;
  presence_animbars=%f //pour animation graphique 2D automatique des bars
  
  //read global time
  tf=scs_m.props.tf;
  
  for i = 1:size(scs_m.objs)
    curObj= scs_m.objs(i);
    if (typeof(curObj) == "Block" & curObj.gui == "ISCOPE")
      presence_scope=%t 
      list_scope($+1)=i;
    elseif (typeof(curObj) == "Block" & curObj.gui == "IREP_TEMP") then
      presence_ireptemp=%t 
      list_ireptemp($+1)=i;
      //update time simulation
      tf=scs_m.objs(list_ireptemp($)).model.rpar(2);
      scs_m.props.tf=tf;
      display_now=evstr(scs_m.objs(list_ireptemp($)).graphics.exprs(4));
    elseif (typeof(curObj) == "Block" & curObj.gui == "AnimBar2D") then
       presence_animbars=%t  
    end
  end  
  
  //security tests
  if presence_ireptemp  & size(list_ireptemp,'*')>1 then
     messagebox('Vous ne devez mettre en place qu''un seul bloc IREP_TEMP');
     continueSimulation = %f;
     return;
  end
  
  if presence_scope & ~presence_ireptemp then
     messagebox('Vous devez insérer un bloc d''analyse (IREP_TEMP) si vous utilisez des blocs ISCOPE pour spécifier la durée de simulation et le nombre de points de calculs');
     continueSimulation = %f;
     return;      
  end
  
  //update ISCOPES
  if presence_scope then
     nb_total_outputs=0;
     nb_objs_in_scopeblock=7;
        for i=1:size(list_scope,1)
            //read data from ISCOPE
           nb_outputs=evstr(scs_m.objs(list_scope(i)).graphics.exprs(1));

           //read data from ireptemp
           obj_ireptemp=scs_m.objs(list_ireptemp(1));
           num_pts=obj_ireptemp.model.rpar(1);
           sample_time=tf/num_pts;
           list_obj=scs_m.objs(list_scope(i)).model.rpar.objs;
           
           if display_now==1 then
           
                no=1;
                scope=CSCOPE('define');
                scope.model.rpar(4)=tf;

                scope.graphics.exprs(7)=string(tf);
                for j=1:size(list_obj)
                    if (typeof(list_obj(j)) == "Block" & list_obj(j).gui == "TOWS_c") then //on affecte un nom pour le stockage dans scilab
                        scope.graphics.pin = scs_m.objs(list_scope(i)).model.rpar.objs(j).graphics.pin;
                        scope.graphics.pein = scs_m.objs(list_scope(i)).model.rpar.objs(j).graphics.pein;
                        scope.graphics.sz=scs_m.objs(list_scope(i)).model.rpar.objs(j).graphics.sz;
                        scs_m.objs(list_scope(i)).model.rpar.objs(j)=scope;
                        no=no+1;
                    elseif (typeof(list_obj(j)) == "Block" & list_obj(j).gui == "SampleCLK") then //on modifie le pas de temps
                        scs_m.objs(list_scope(i)).model.rpar.objs(j).model.rpar(1)=sample_time;
                        scs_m.objs(list_scope(i)).model.rpar.objs(j).graphics.exprs(1)=string(sample_time);                        
                    end
                end
            else
                no=1;
                for j=1:size(list_obj)
                    if (typeof(list_obj(j)) == "Block" & list_obj(j).gui == "TOWS_c") then //on affecte un nom pour le stockage dans scilab
                        scs_m.objs(list_scope(i)).model.rpar.objs(j).graphics.exprs=[string(num_pts);"o"+string(no+nb_total_outputs);"0"];
                        scs_m.objs(list_scope(i)).model.rpar.objs(j).model.ipar=[num_pts;2;24;no+nb_total_outputs]; 
                        no=no+1;
                    elseif (typeof(list_obj(j)) == "Block" & list_obj(j).gui == "SampleCLK") then //on modifie le pas de temps
                        scs_m.objs(list_scope(i)).model.rpar.objs(j).model.rpar(1)=sample_time;          
                        scs_m.objs(list_scope(i)).model.rpar.objs(j).graphics.exprs(1)=string(sample_time);                                                
                    end
                end
                
            end
            
           nb_total_outputs=nb_total_outputs+nb_outputs;
      end
  end 
  
  if presence_animbars then
      //scs_m=animate_bars_xy(scs_m)
      disp("Anim Bar 2D")
  end
  
  disp("Fin SIMM pre_simulate")
  scs_m=resume(scs_m);

endfunction
