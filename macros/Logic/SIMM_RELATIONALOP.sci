//
// 22/06/2016 - D. Violeau
// Bloc permettant de faire des comparaisons entre deux signaux

function [x,y,typ]=SIMM_RELATIONALOP(job,arg1,arg2)
    x=[];
    y=[];
    typ=[];
    select job
    case "set" then
        x=arg1;
        graphics=arg1.graphics;
        exprs=graphics.exprs
        model=arg1.model;

        while %t do
            [ok,rule,exprs]=scicos_getvalue(__("Set parameters"),..
            ["Operator: == (0), ~= (1), < (2), <= (3), > (4), >= (5)"],..
            list("vec",1),exprs)
            if ~ok then
                break,
            end

            rule=int(rule);

            if (rule<0)|(rule>5) then
                message(__("Incorrect operator "+string(rule)+" ; must be 0 to 5."));
                ok=%f
            end


            if ok then
                nin=2
                it=ones(nin,1);
                ot=1;
                in=[ones(nin,1) ones(nin,1)]
                out=[1 1]
                [model,graphics,ok]=set_io(model,graphics,list(in,it),list(out,ot),[],[])
                model.sim='SIMM';
                    graphics.in_implicit=['I';'I'];
                    graphics.in_style=[RealInputStyle();RealInputStyle()];
                    graphics.out_implicit=['I'];
                    graphics.out_style=[RealOutputStyle()]; 

                    mo=modelica();
                    disp(rule)
                    if rule==0 then 
                        mo.model='SIMM.EGAL';
                    elseif rule==1 then
                        mo.model='SIMM.DIFF';
                    elseif rule==2 then
                        mo.model='SIMM.INF';
                    elseif rule==3 then
                        mo.model='SIMM.INFEGAL';
                    elseif rule==4 then
                        mo.model='SIMM.SUP';
                     elseif rule==5 then
                        mo.model='SIMM.SUPEGAL';
                    end
                    mo.inputs=['u1';'u2'];
                    mo.outputs=['y'];
                    model.equations=mo;                       
            end
            if ok then
if rule == 0 then
                    label = "==";
                elseif rule == 1 then
                    label = "~=";
                elseif rule == 2 then
                    label = "&#60;"; // <
                elseif rule == 3 then
                    label = "&#8804;"; // <=
                elseif rule == 4 then
                    label = "&#62;"; // >
                elseif rule == 5 then
                    label = "&#8805;"; // >=
                end
                graphics.exprs=exprs;
                graphics.style = ["blockWithLabel;displayedLabel="+label];
                x.graphics=graphics;
                x.model=model
                break
            end
        end
    case "define" then

        rule=2
        model=scicos_model();
        model.sim='SIMM';
        model.blocktype='c';
        model.dep_ut=[%t %f];
        mo=modelica();
        mo.model='SIMM.INF';
        mo.inputs=['u1','u2'];
        mo.outputs=['y'];
        model.equations=mo;
        model.in=ones(size(mo.inputs,'*'),1);
        model.out=ones(size(mo.outputs,'*'),1);

        exprs=[string([rule])]
        gr_i=[]
        x=standard_define([2 2],model,exprs,gr_i)
        x.graphics.in_implicit=['I';'I'];
        x.graphics.in_style=[RealInputStyle();RealInputStyle()];
        x.graphics.out_implicit=['I'];
        x.graphics.out_style=[RealOutputStyle()];
        if rule == 0 then
                    label = "==";
                elseif rule == 1 then
                    label = "~=";
                elseif rule == 2 then
                    label = "&#60;"; // <
                elseif rule == 3 then
                    label = "&#8804;"; // <=
                elseif rule == 4 then
                    label = "&#62;"; // >
                elseif rule == 5 then
                    label = "&#8805;"; // >=
                end

        x.graphics.style = ["blockWithLabel;displayedLabel="+label];
    end
endfunction


//function [x,y,typ] = SIMM_RELATIONALOP(job,arg1,arg2)
//    x=[];
//    y=[];
//    typ=[];
//    select job
//    case "set" then
//        x=arg1;
//        graphics=arg1.graphics;
//        exprs=graphics.exprs
//        model=arg1.model;
//
//        while %t do
//            [ok,rule,exprs]=scicos_getvalue(__("Set parameters"),..
//            [__("Operator: == (0), ~= (1), < (2), <= (3), > (4), >= (5)")],..
//            list("vec",1),exprs)
//            if ~ok then
//                break,
//            end
//            rule=int(rule);
//            if (rule<0)|(rule>5) then
//                message("Incorrect operator "+string(rule)+" ; must be 0 to 5.")
//                ok=%f;
//            end
//
//            Datatype=1
//            model.sim=list("relational_op",4)
//            if ok then
//                it=Datatype*ones(1,2)
//                ot=Datatype
//                in=[-1 -2;-1 -2]
//                out=[-1 -2]
//                [model,graphics,ok]=set_io(model,graphics,list(in,it),list(out,ot),[],[])
//            end
//            if ok then
//                if rule == 0 then
//                    label = "==";
//                elseif rule == 1 then
//                    label = "~=";
//                elseif rule == 2 then
//                    label = "&#60;"; // <
//                elseif rule == 3 then
//                    label = "&#8804;"; // <=
//                elseif rule == 4 then
//                    label = "&#62;"; // >
//                elseif rule == 5 then
//                    label = "&#8805;"; // >=
//                end
//                graphics.exprs=exprs;
//                graphics.style=["fontSize=13;fontStyle=1;displayedLabel="+label];
//                model.ipar=[rule],
//                model.nzcross=zcr,
//                model.nmode=zcr,
//                x.graphics=graphics;
//                x.model=model
//                break
//            end
//        end
//    case "define" then
//        ipar=[2]
//        label="&lt";
//        model=scicos_model()
//        model.sim=list("relationalop",4)
//        model.in=[1;1]
//        model.out=1
//        model.ipar=ipar
//        model.blocktype="c"
//        model.dep_ut=[%t %f]
//        exprs=[string(ipar);string(0)]
//        gr_i=[]
//        x=standard_define([2 2],model,exprs,gr_i)
//        x.graphics.style=["fontSize=13;fontStyle=1;displayedLabel="+label];
//    end
//endfunction
//
