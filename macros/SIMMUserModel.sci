//  Scicos
//
//  Copyright (C) INRIA - METALAU Project <scicos@inria.fr>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//
// See the file ../license.txt
//

function [x,y,typ]=SIMMUserModel(job,arg1,arg2)
    x=[];
    y=[];
    typ=[];
    select job
    case "set" then
        x=arg1
        model=arg1.model
        graphics=arg1.graphics
        //label=graphics.exprs;
        exprs=graphics.exprs

        //Compatibility
        if type(exprs)==15 then
            paramv=list();
            pprop=[];
            //must do something better than that !
            for i=1:size(model.rpar,"*")
                paramv($+1)=string(model.rpar(i))
                pprop($+1) = 0
            end

            exprs = tlist(["MBLOCK","in","intype","out","outtype",...
            "param","paramv","nameF","funtxt"],...
            exprs(1)(1),..
            exprs(1)(2),..
            exprs(1)(3),..
            exprs(1)(4),..
            exprs(1)(5),..
            paramv,...
            exprs(1)(6),exprs(2))

        end

        //lab_1 = [in,intype,out,outtype,param,nameF]
        lab_1 = list(exprs.in,..       //1
        exprs.intype,..   //2
        exprs.out,..      //3
        exprs.outtype,..  //4
        exprs.param,..    //5
        exprs.nameF)      //7

        //lab_2 = [paramv]
        lab_2 = exprs.paramv //already a list

        //store exprs before modif
        //exprs.prev_exprs=exprs;
        //exprs.prev_exprs.prev_exprs=tlist("MBLOCK");

        while %t do
            //     [ok,Tin,Tintype,Tout,Touttype,Tparam,Tparamv,Tfunam,lab]=..
            //         scicos_getvalue('Set Modelica generic block parameters',..
            //         ['Input variables:       ';..
            //          'Input variables types: ';..
            //          'Output variables:      ';..
            //          'Output variables types:';..
            //          'Parameters in Modelica:';..
            //          'Parameter  values:     ';..
            //          'Function name:         '],..
            //         list('str',-1,'str',-1,'str',-1,'str',-1,'str',-1,'str',-1,'str',-1),label(1))

            //warning here lab_1 is a list

            [ok,Tin,Tintype,Tout,Touttype,Tparam,Tfunam,lab_1]=..
            scicos_getvalue("Choisir les parametres de definition du bloc personnel",..
            ["Nom des connecteurs d''entrée :       ";..
            "Types de connecteur (voir aide, ex El+ pour électrique plein): ";..
            "Nom des connecteurs de sortie :      ";..
            "Types de connecteur (voir aide, ex El- pour électrique vide):";..
            "Nom des parametres dans la fonction :";..
            "Nom de la fonction :         "],..
            list("str",-1,"str",-1,"str",-1,"str",-1,..
            "str",-1,"str",-1),lab_1)
            param=stripblanks(evstr(Tparam))
            pprop=zeros(param)

            //Cancel case
            if ~ok then
                break
            end
            //   ierr=execstr("in=stripblanks(evstr(Tin));..
            //   intype=stripblanks(evstr(Tintype));..
            //   out=stripblanks(evstr(Tout));..
            //   outtype=stripblanks(evstr(Touttype));..
            //   param=stripblanks(evstr(Tparam));..
            //   paramv=evstr(Tparamv);..
            //   funam=stripblanks(Tfunam)",'errcatch')
            //
            //first stage of checking variable name
            //put string symbolic parameters in variable
            ierr=execstr("in=stripblanks(evstr(Tin));..
            intype=stripblanks(evstr(Tintype));..
            out=stripblanks(evstr(Tout));..
            outtype=stripblanks(evstr(Touttype));..
            param=stripblanks(evstr(Tparam));..
            funam=stripblanks(Tfunam)","errcatch")

            if ierr<>0 then
                //You lose! Try again! Insert coin!
                //messagebox("Error, try again please!","modal","error");
                messagebox("Error in evaluation of variables.","modal","error");
                ok=%f
                //break
            end

            //check for valid name variable
            //in
            if ok then
                for i=1:size(in,"*")
                    r=%f;
                    ierr=execstr("r=validvar(in(i))","errcatch")
                    if ~r then
                        ok=%f
                        break
                    end
                end
                if ~ok then
                    messagebox(["Invalid variable name for the input "+string(i)+".";
                    """"+in(i)+"""";
                    "Please choose another variable name."],"modal","error");
                end
            end
            //out
            if ok then
                for i=1:size(out,"*")
                    r=%f;
                    ierr=execstr("r=validvar(out(i))","errcatch")
                    if ~r then
                        ok=%f
                        break
                    end
                end
                if ~ok then
                    messagebox(["Invalid variable name for the output "+string(i)+".";
                    """"+out(i)+"""";
                    "Please choose another variable name."],"modal","error");
                end
            end
            //param
            if ok then
                param=param(:)
                for i=1:size(param,"*")
                    r=%f;
                    ierr=execstr("r=validvar(param(i))","errcatch")
                    if ~r then
                        ok=%f
                        break
                    end
                end
                if ~ok then
                    messagebox(["Invalid variable name for the parameter "+string(i)+".";
                    """"+param(i)+"""";
                    "Please choose another variable name."],"modal","error");
                end
            end

            // type checking
            //typeok=%t;
            if ok then
                list_type1=["El","Hy","Ae","Ro","Tr","Pl","Th","Si"]
                list_typeplus1=list_type1+"+"
                list_typemoins1=list_type1+"-"

                for i=1:size(intype,"*")

                    a=find(intype(i)==list_typeplus1)
                    b=find(intype(i)==list_typemoins1)
                    c=(intype(i)=="E" | intype(i)=="I")

                    if isempty(a) & isempty(b) & c==%f then
                        //typeok=%f;
                        messagebox("Les types d''entrées sont El, Hy, Ae,Ro, Tr, Pl, Th, Si avec le symbole + ou - ou bien E, I","modal","error");
                        ok=%f
                        break
                    end
                end
            end
            //    if ~typeok then
            //      ok=%f
            //      //break
            //    end

            if ok then
                list_type=["El","Hy","Ae","Ro","Tr","Pl","Th","Si"]
                list_typeplus=list_type+"+"
                list_typemoins=list_type+"-"

                for i=1:size(outtype,"*")
                    a=find(outtype(i)==list_typeplus)
                    b=find(outtype(i)==list_typemoins)
                    c=(outtype(i)=="E" | outtype(i)=="I")

                    if isempty(a) & isempty(b) & c==%f then
                        //typeok=%f;
                        messagebox("Les types de sorties sont El, Hy, Ae,Ro, Tr, Pl, Th, Si avec le symbole + ou - ou bien E, I","modal","error");
                        ok=%f
                        break
                    end
                end
            end
            //   if ~typeok then break, end

            //cross size checking
            if ok then
                if or(size(intype)<>size(in)) then
                    messagebox("Input variables are not well defined!","modal","error");
                    ok=%f
                    //break;
                end
            end

            if ok then
                if or(size(outtype)<>size(out)) then
                    messagebox("Output variables are not well defined!","modal","error");
                    ok=%f
                    //break;
                end
            end

            //check param properties
            if ok then
                pprop = pprop(:)
                if (size(param,"*")<>size(pprop,"*")) then
                    messagebox(["There is differences in";
                    "size of param and size ";
                    "of param properties."],"modal","error");
                    ok=%f
                    //break;
                end
            end

            if ok then
                if max(pprop)>2 | min(pprop)<0 then
                    messagebox(["Parameters properties must be :";
                    "0 : for simple paramater,";
                    "1 : for initial state value,";
                    "2 : for a fixed initial state value."],"modal","error");
                    ok=%f
                    //break;
                end
            end

            //check name of modelica file
            if ok then
                if funam=="" then
                    messagebox("The filename is not defined!","modal","error");
                    ok=%f
                    //break
                end
            end

            if ok then
                [dirF,nameF,extF]=fileparts(funam);
                if (extF<>"" & extF<>".mo")|(dirF<>"" & extF<>".mo") then
                    messagebox("Filename extention should be ''.mo '' !","modal","error");
                    ok=%f
                    //break
                end
            end

            if ok then
                //set_io checking
                //intypex=find(intype~="E"); outtypex=find(outtype~="E"); 
                intypex=[1:size(intype,"*")]
                outtypex=[1:size(outtype,"*")]
                //[model,graphics,ok]=check_io(model,graphics,ones(in),..
                //                             ones(out),[],[],intypex,outtypex)
                [model,graphics,ok]=set_io(model,graphics,list([ones(in),ones(in)],ones(in)),..
                list([ones(out),ones(out)],ones(out)),..
                [],[],intypex,outtypex)
                // TODO
                //changer le style 

                list_styles=["El","Hy","Ae","Ro","Tr","Pl","Th","Si","E","I"]
                list_styles_plus=[ElecInputStyle(),HydraulicInputStyle(),AeraulicInputStyle(),RotInputStyle(),TransInputStyle(),PlanInputStyle(),ThermalInputStyle(),RealInputStyle(),ExplicitStyle(),ImplicitStyle()];
                list_styles_moins=[ElecOutputStyle(),HydraulicOutputStyle(),AeraulicOutputStyle(),RotOutputStyle(),TransOutputStyle(),PlanOutputStyle(),ThermalOutputStyle(),RealOutputStyle()];

                    
                for i =[1:size(intypex,'*')]

                    if intype(i)=="E" then
                        graphics.in_implicit(intypex(i))="E"
                        repp=find(list_styles==intype(intypex(i)))
                    elseif intype(i)=="I" then
                        graphics.in_implicit(intypex(i))="I"
                        repp=find(list_styles==intype(intypex(i)))
                    else
                        graphics.in_implicit(intypex(i))="I"
                        repp=find(list_styles+"+"==intype(intypex(i)))
                        repm=find(list_styles+"-"==intype(intypex(i)))
                    end


                    if ~isempty(repp) then 
                        graphics.in_style(i)=list_styles_plus(repp)
                    elseif ~isempty(repm) then 
                        graphics.in_style(i)=list_styles_moins(repm) 
                    end

                end
                for i =[1:size(outtypex,'*')]

                    if outtype(i)=="E" then
                        graphics.out_implicit(outtypex(i))="E"
                        repp=find(list_styles==outtype(outtypex(i)))
                    elseif outtype(i)=="I" then
                        graphics.out_implicit(outtypex(i))="I"
                        repp=find(list_styles==outtype(outtypex(i)))
                     else
                        graphics.out_implicit(outtypex(i))="I"
                        repp=find(list_styles+"+"==outtype(i))
                        repm=find(list_styles+"-"==outtype(i))
                    end

                    if ~isempty(repp) then 
                        graphics.out_style(i)=list_styles_plus(repp)
                    elseif ~isempty(repm) then 
                        graphics.out_style(i)=list_styles_moins(repm) 
                    end
                end
                
            end

            //*****************************************************************************
            //generate second dialog box from Tparam
            if ok then
                Tparam_lab=evstr(Tparam) //labels
                Tparam_sz=size(Tparam_lab,"*") //numbers of labels
                //adjust size of lab_2 according to size of Tparam
                if Tparam_sz>lstsize(lab_2) then
                    for i=1:(Tparam_sz-lstsize(lab_2))
                        lab_2($+1)="0"
                    end
                elseif Tparam_sz<lstsize(lab_2) then
                    lab_2_tmp=list()
                    if Tparam_sz<>0 then //if param
                        for i=1:(Tparam_sz)
                        //for i=1:(lstsize(lab_2)-Tparam_sz)
                            lab_2_tmp(i)=lab_2(i)
                        end
                    end
                    lab_2=lab_2_tmp
                end

                //generate lhs, label and rhs txt for scicos_getvalue
                if Tparam_sz<>0 then //if param
                    lhs_txt="" //lhs txt
                    lab_txt="" //label txt
                    rhs_txt="" //rhs txt
                    for i=1:Tparam_sz
                        lhs_txt=lhs_txt+"%v"+string(i)+","
                        if pprop(i) == 0 then
                            lab_txt=lab_txt+"''"+Tparam_lab(i)+"'';"
                        elseif  pprop(i) == 1 then
                            lab_txt=lab_txt+"''"+Tparam_lab(i)+" (state) '';"
                        elseif  pprop(i) == 2 then
                            lab_txt=lab_txt+"''"+Tparam_lab(i)+" (fixed state) '';"
                        end
                        rhs_txt=rhs_txt+"''vec'',-1,"
                    end
                    lhs_txt=part(lhs_txt,1:length(lhs_txt)-1)
                    lab_txt=part(lab_txt,1:length(lab_txt)-1)
                    rhs_txt=part(rhs_txt,1:length(rhs_txt)-1)

                    //generate main scicos_getvalue cmd
                    //warning here lab_2 is a list in input and a string in output
                    getvalue_txt = "[ok,"+lhs_txt+",lab_2]=scicos_getvalue(''Set parameters values'',["+..
                    lab_txt+"],"+..
                    "list("+rhs_txt+"),lab_2)"

                    //display the second dialog box
                    execstr(getvalue_txt)

                    //restore original lab_2 if not ok
                    if ~ok then
                        lab_2 = exprs.paramv
                    end
                end
            end

            //put output param in the form of a list
            //in paramv
            if ok then
                paramv=list();
                for i=1:Tparam_sz
                    execstr("paramv("+string(i)+")=%v"+string(i))
                end
            end
            //*****************************************************************************

            //============================---------MBLOCK2
            if ok then
                if extF==".mo" & fileinfo(funam)<>[] then
                    tt=mgetl(funam)
                else
                    tt=exprs.funtxt
                    mo=model.equations
                    //if model.sim(1)<>nameF then
                    if mo.model<>nameF then
                        tt=[]
                    end
                end
                //============================
                [ok,tt]=SIMMCOM(funam,tt,in,out,param,paramv,pprop,intype,outtype)
                if ~ok then
                    break
                end
            end

            if ok then
                //------------------

                mo=modelica()
                mo.model=nameF
                mo.inputs=in;

                mo.outputs=out;
                if max(pprop)>0 then
                    mo.parameters=list(param',paramv,pprop')
                else
                    mo.parameters=list(param',paramv)
                end
                model.equations=mo
                //------------------
                //model.rpar=paramv;
                model.rpar=[]
                for i=1:lstsize(paramv)
                    model.rpar=[model.rpar;
                    paramv(i)(:)]//value
                end
                model.sim(1)=funam
                //------------------
                exprs.in=lab_1(1)
                exprs.intype=lab_1(2)
                exprs.out=lab_1(3)
                exprs.outtype=lab_1(4)
                exprs.param=lab_1(5)
                exprs.paramv=list();
                if Tparam_sz<>0 then //if param
                    if type(lab_2)==15 then //for eval
                        for i=1:lstsize(lab_2)
                            exprs.paramv(i)=lab_2(i);//string
                        end
                    else
                        for i=1:size(lab_2,"*")
                            exprs.paramv(i)=lab_2(i);
                        end
                    end
                end
                //exprs.pprop=lab_1(6)
                exprs.nameF=lab_1(6)
                exprs.funtxt=tt
                //label(2)=tt
                //------------------
                x.model=model
                graphics.gr_i(1)(1)="txt=[''Modelica'';'' " + nameF + " ''];"
                //graphics.in_implicit =intype
                //graphics.out_implicit=outtype
                //graphics.exprs=label
                graphics.exprs=exprs
                //graphics.style=["blockWithLabel;spacingBottom=2;verticalLabelPosition=bottom;displayedLabel="+nameF]
                x.graphics=graphics;
                x.graphics.in_label=in;
                x.graphics.out_label=out;
                x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel="+Tfunam]
                break
            end
        end

        //Define
    case "define" then
        in=["u";"p";'n"] 
        intype=["Si+";"El+";"El-"] //type multiphysique à gauche du bloc ajouter + ou - pour differencier un connecteur plein ou vide : El : Electrique, Ex : Explicite Scilab, Im : implicite Scilab, Hy : hydraulique, Ae : Aeraulique, Ro : Rotation, Tr : Translation, Pl : Plane, Th : Thermique, Si : Signal
        out=["rot"] // Idem pour les types à droite du bloc
        outtype=["Ro-"]
        param=["R";"k";"J"];
        //paramv=[1;0.01;0.0001];
        paramv=list(1,.01,.0001)
        pprop=[0;0;0];
        nameF="generic"

        //label=list([sci2exp(in),sci2exp(intype,sci2exp(out),..
        //           sci2exp(outtype),sci2exp(param),sci2exp(paramv),nameF]',[])

        exprs = tlist(["MBLOCK","in","intype","out","outtype",...
        "param","paramv","nameF","funtxt"],...
        sci2exp(in(:)),..
        sci2exp(intype(:)),..
        sci2exp(out(:)),..
        sci2exp(outtype(:)),..
        sci2exp(param(:)),..
        list(string(1),string(.01),string(.0001)),...
        nameF,[])

        model=scicos_model()
        model.blocktype="c"
        model.dep_ut=[%f %t]
        //model.rpar=paramv;
        model.rpar=[]
        for i=1:lstsize(paramv)
            model.rpar=[model.rpar;
            paramv(i)(:)]
        end

        mo=modelica()
        mo.model=nameF
        mo.parameters=list(param,paramv)
        model.sim=list(mo.model,30004)

        //les noms des entrees sont forcement connus en fonction de leur type multiphysique
        //
        mo.inputs=in
        mo.outputs=out
        model.in=ones(size(mo.inputs,"r"),1)
        model.out=ones(size(mo.outputs,"r"),1)
        model.equations=mo
        gr_i=[]
        //x=standard_define([3 2],model,label,gr_i)
        x=standard_define([4 4],model,exprs,gr_i)
        x.graphics.in_implicit =["I";"I";"I"]
        x.graphics.in_style=[RealInputStyle();ElecInputStyle();ElecOutputStyle()]
        x.graphics.out_implicit=["I"]
        x.graphics.out_style=[RotOutputStyle()]
        x.graphics.in_label=in;
        x.graphics.out_label=out;
        x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel="+"generic"]
    end
endfunction

