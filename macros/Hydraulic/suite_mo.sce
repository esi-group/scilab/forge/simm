

  end Components;
  package Examples
    package BasicComponents
      model Sources
        Components.Sources.FlowVolumetric debit ;
        Components.Sources.Pressure pression ;
        Modelica.Blocks.Sources.Constant const(k = 500000.0) ;
        Modelica.Blocks.Sources.Constant const1(k = 1) ;
      equation
        connect(pression.port_B,debit.port_B) ;
        connect(const.y,pression.signal) ;
        connect(debit.signal,const1.y) ;
      end Sources;
      model Restriction
        Modelica.Blocks.Sources.Constant const(k = 500000.0) ;
        Modelica.Blocks.Sources.Constant const1(k = 1) ;
        Components.Sources.Pressure pression ;
        Components.Restrictions.Laminar laminar(G = 1 / 250000.0) ;
        Components.Sources.FlowVolumetric debit ;
      equation
        connect(const.y,pression.signal) ;
        connect(pression.port_B,laminar.portA) ;
        connect(laminar.portB,debit.port_B) ;
        connect(debit.signal,const1.y) ;
      end Restriction;
      model Reservoir
        Modelica.Blocks.Sources.Constant const(k = 500000.0) ;
        Components.Sources.Pressure pression1 ;
        Components.Restrictions.Laminar laminar(G = 1 / 250000.0) ;
        Components.Reservoir reservoir1 ;
      equation
        connect(pression1.signal,const.y) ;
        connect(laminar.portA,pression1.port_B) ;
        connect(laminar.portB,reservoir1.portA) ;
      end Reservoir;
      model Volume "Test du volume"
        Hydraulic.Parts.ChamberLeft chamber(PistonArea = 1) ;
        Components.Sources.Pressure pressure ;
        Modelica.Blocks.Sources.Constant const(k = 100000.0) ;
        Modelica.Mechanics.Translational.Components.Fixed fixed ;
        Modelica.Mechanics.Translational.Components.Fixed fixed1 ;
      equation
        connect(pressure.port_B,chamber.port_A) ;
        connect(const.y,pressure.signal) ;
        connect(fixed.flange,chamber.flange_a) ;
        connect(fixed1.flange,chamber.flange_b) ;
      end Volume;
      model Pump
        Parts.RotComp pompeConstante ;
        Components.Reservoir reservoir ;
        Components.Sources.FlowVolumetric debit ;
        Modelica.Blocks.Sources.Constant const(k = 1) ;
        Modelica.Blocks.Continuous.SecondOrder secondOrder(k = 1, w = 1, D = 1) ;
        Modelica.Mechanics.Rotational.Components.Damper damper(d = 1) ;
        Modelica.Mechanics.Rotational.Components.Fixed fixed ;
      equation
        connect(debit.port_B,pompeConstante.portA) ;
        connect(pompeConstante.portB,reservoir.portA) ;
        connect(const.y,secondOrder.u) ;
        connect(secondOrder.y,debit.signal) ;
        connect(pompeConstante.flange,damper.flange_a) ;
        connect(damper.flange_b,fixed.flange) ;
      end Pump;
      model RendementVol
        Modelica.Blocks.Sources.Sine const(freqHz = 1) ;
        Components.Sources.FlowVolumetric SourceDebit_A ;
        Components.Sources.FlowVolumetric pressure_C ;
        Components.Sources.Pressure pressure_B ;
        Modelica.Blocks.Sources.Sine const1(freqHz = 1, phase = 1.5707963267949) ;
        Modelica.Blocks.Sources.Sine const2(freqHz = 0.5) ;
        Parts.VolEfficiency volEfficiency ;
      equation
        connect(SourceDebit_A.signal,const.y) ;
        connect(pressure_B.signal,const1.y) ;
        connect(const2.y,pressure_C.signal) ;
        connect(SourceDebit_A.port_B,volEfficiency.portA) ;
        connect(volEfficiency.portB,pressure_B.port_B) ;
        connect(pressure_C.port_B,volEfficiency.portC) ;
      end RendementVol;
      model PumpMotor
        Components.PumpMotor PompeMoteur(Cyl = 0.000001) ;
        Components.Reservoir reservoir ;
        Components.Sources.FlowVolumetric debit ;
        Modelica.Blocks.Sources.Constant const(k = 1) ;
        Modelica.Blocks.Continuous.SecondOrder secondOrder(k = 1, w = 1, D = 1) ;
        Modelica.Mechanics.Rotational.Components.Damper damper(d = 1) ;
        Modelica.Mechanics.Rotational.Components.Fixed fixed ;
      equation
        connect(debit.port_B,PompeMoteur.portA) ;
        connect(PompeMoteur.portB,reservoir.portA) ;
        connect(const.y,secondOrder.u) ;
        connect(secondOrder.y,debit.signal) ;
        connect(PompeMoteur.flange,damper.flange_a) ;
        connect(damper.flange_b,fixed.flange) ;
      end PumpMotor;
      model Cylinder
        Modelica.Blocks.Sources.Constant const(k = 500000.0) ;
        Components.Cylinder VerinBloque ;
        Modelica.Mechanics.Translational.Components.Fixed fixed ;
        Components.Cylinder VerinLibre ;
        Components.Sources.FlowVolumetric flowVolumetric ;
        Modelica.Blocks.Sources.Sine sine(amplitude = 1, freqHz = 1) ;
        Components.Reservoir reservoir1 ;
        Components.Reservoir reservoir ;
        Hydraulic.Sources.Pressure pressure1 ;
      equation
        connect(reservoir.portA,VerinBloque.port_A2) ;
        connect(const.y,pressure.signal) ;
        connect(VerinBloque.flange_b,fixed.flange) ;
        connect(reservoir1.portA,VerinLibre.port_A2) ;
        connect(flowVolumetric.port_B,VerinLibre.port_A1) ;
        connect(sine.y,flowVolumetric.signal) ;
        connect(pressure.port_B,VerinBloque.port_A1) ;
      end Cylinder;
      model Restriction_turbulent
        Modelica.Blocks.Sources.Constant const(k = 7000000.0) ;
        Components.Sources.Pressure pression ;
        Components.Restrictions.Turbulent turbulent(Qnom(displayUnit = "l/min") = 0.000016666666666667) ;
        Components.Reservoir reservoir ;
      equation
        connect(const.y,pression.signal) ;
        connect(pression.port_B,turbulent.portA) ;
        connect(turbulent.portB,reservoir.portA) ;
      end Restriction_turbulent;
      model Restriction_turbulent_controled
        Modelica.Blocks.Sources.Constant const(k = 7000000.0) ;
        Components.Sources.Pressure pression ;
        Components.Restrictions.TurbulentControled turbulent(Qnom(displayUnit = "l/min") = 0.000016666666666667, Imax = 1) ;
        Components.Reservoir reservoir ;
        Modelica.Blocks.Sources.Sine sine(freqHz = 1, amplitude = 1) ;
      equation
        connect(const.y,pression.signal) ;
        connect(pression.port_B,turbulent.portA) ;
        connect(turbulent.portB,reservoir.portA) ;
        connect(sine.y,turbulent.i) ;
      end Restriction_turbulent_controled;
      model Valve33
        Modelica.Blocks.Sources.Constant const(k = 7000000.0) ;
        Components.Sources.Pressure pression ;
        Hydraulic.Components.Valve33 turbulent(Qnom(displayUnit = "l/min") = 0.000016666666666667, Imax = 1) ;
        Components.Reservoir reservoir ;
        Modelica.Blocks.Sources.Sine sine(freqHz = 1, amplitude = 1);
        Modelica.Blocks.Sources.Constant const1(k = 3500000.0) ;
        Components.Sources.Pressure pression1 ;
      equation
        connect(const.y,pression.signal) ;
        connect(const1.y,pression1.signal) ;
        connect(pression1.port_B,turbulent.portA) ;
        connect(sine.y,turbulent.i1) ;
        connect(pression.port_B,turbulent.port_P) ;
        connect(reservoir.portA,turbulent.port_T) ;
      end Valve33;
      model Valve43
        Modelica.Blocks.Sources.Constant const(k = 7000000.0) ;
        Components.Sources.Pressure Psource ;
        Components.Valve43 turbulent(Qnom(displayUnit = "l/min") = 0.000016666666666667, Imax = 1);
        Components.Reservoir reservoir ;
        Modelica.Blocks.Sources.Sine sine(freqHz = 1, amplitude = 1) ;
        Modelica.Blocks.Sources.Constant const1(k = 3500000.0) ;
        Components.Sources.Pressure V_A ;
        Modelica.Blocks.Sources.Constant const2(k = 20) ;
        Components.Sources.Pressure V_B ;
      equation
        connect(const.y,Psource.signal) ;
        connect(const1.y,V_A.signal) ;
        connect(V_A.port_B,turbulent.portA) ;
        connect(sine.y,turbulent.i1) ;
        connect(Psource.port_B,turbulent.port_P) ;
        connect(turbulent.port_T,reservoir.portA) ;
        connect(V_B.port_B,turbulent.portB) ;
        connect(V_B.signal,const2.y) ;
      end Valve43;
      model Valve43_cc
        Modelica.Blocks.Sources.Constant const(k = 7000000.0) ;
        Components.Sources.Pressure Psource ;
        Components.Valve43 turbulent(Qnom(displayUnit = "l/min") = 0.000016666666666667, Imax = 1) ;
        Components.Reservoir reservoir ;
        Modelica.Blocks.Sources.Sine sine(freqHz = 1, amplitude = 1) ;
      equation
        connect(const.y,Psource.signal) ;
        connect(sine.y,turbulent.i1) ;
        connect(Psource.port_B,turbulent.port_P) ;
        connect(turbulent.port_T,reservoir.portA) ;
        connect(turbulent.portA,turbulent.portB) ;
      end Valve43_cc;
      model Cylinder_2
        Modelica.Blocks.Sources.Constant const(k = 500000.0) ;
        Components.Cylinder VerinBloque ;
        Components.Sources.Pressure pressure ;
        Modelica.Mechanics.Translational.Components.Fixed fixed ;
        Components.Sources.Pressure pressure1 ;
        Modelica.Blocks.Sources.Constant const1(k = -500000.0) ;
      equation
        connect(pressure.port_B,VerinBloque.port_A1) ;
        connect(const.y,pressure.signal) ;
        connect(VerinBloque.flange_b,fixed.flange);
        connect(pressure1.port_B,VerinBloque.port_A2) ;
        connect(const1.y,pressure1.signal) ;
      end Cylinder_2;
      model RendementRot
        Modelica.Blocks.Sources.Sine const(freqHz = 1) ;
        Parts.Rotational.RotationalEfficiency rotationalEfficiency(eta = 0.6) ;
        Modelica.Mechanics.Rotational.Sources.Torque torque ;
        Modelica.Mechanics.Rotational.Components.Inertia inertia(J = 1) ;
      equation
        connect(const.y,torque.tau) ;
        connect(rotationalEfficiency.flange_b,inertia.flange_a) ;
        connect(torque.flange,rotationalEfficiency.flange_a) ;
      end RendementRot;
      model RendementVol_cap
        Modelica.Blocks.Sources.Sine const(freqHz = 1) ;
        Components.Sources.FlowVolumetric SourceDebit_A ;
        Components.Sources.FlowVolumetric pressure_C ;
        Components.Sources.Pressure pressure_B ;
        Modelica.Blocks.Sources.Sine const1(freqHz = 1, amplitude = 100000.0, phase = 1.5707963267949) ;
        Modelica.Blocks.Sources.Sine const2(freqHz = 0.5, amplitude = 0.05) ;
        Parts.VolEfficiency volEfficiency ;
        Parts.HydraulicCapacity hydraulicCapacity(Ch = 0.00001);
      equation
        connect(SourceDebit_A.signal,const.y) ;
        connect(pressure_B.signal,const1.y) ;
        connect(const2.y,pressure_C.signal) ;
        connect(SourceDebit_A.port_B,volEfficiency.portA) ;
        connect(volEfficiency.portB,pressure_B.port_B) ;
        connect(pressure_C.port_B,volEfficiency.portC) ;
        connect(hydraulicCapacity.port_A,volEfficiency.portC) ;
      end RendementVol_cap;
      model HydraulicCapacity
        Parts.HydraulicCapacity hydraulicCapacity(Ch = 0.00001) ;
        Components.Sources.FlowVolumetric flow_source ;
        Modelica.Blocks.Sources.Sine const2(freqHz = 0.5, amplitude = 0.05) ;
      equation
        connect(flow_source.port_B,hydraulicCapacity.port_A) ;
        connect(flow_source.signal,const2.y) ;
      end HydraulicCapacity;
    end BasicComponents;
    
    model DIRAVI_mission_valve_direct_control
      inner MissionProfil.Simulation simulation(StopTime = 60, NumberOfIntervals = 600) annotation(Placement(transformation(extent = {{-82,78},{-62,98}})));
      Components.Cylinder cylinder(MobileMass = 1, PistonAreaLeft = 0.002) annotation(Placement(transformation(extent = {{30,18},{60,48}})));
      Components.Sources.Pressure pressure annotation(Placement(transformation(extent = {{8,-44},{28,-24}})));
      Modelica.Blocks.Sources.Constant const1(k = 17000000.0) annotation(Placement(transformation(extent = {{-38,-44},{-18,-24}})));
      Modelica.Blocks.Sources.Sine sine2(freqHz = 1, amplitude = 0.2, offset = 0) annotation(Placement(transformation(extent = {{-98,-10},{-78,10}})));
      Modelica.Blocks.Math.Feedback feedback_position annotation(Placement(transformation(extent = {{-70,10},{-50,-10}})));
      Modelica.Blocks.Math.Gain gain3(k = -10) annotation(Placement(transformation(extent = {{-10,-10},{10,10}}, rotation = 0, origin = {-28,0})));
      Components.Valve33 valve33_1(Qnom = 0.0016666666666667) annotation(Placement(transformation(extent = {{18,-10},{38,10}})));
      Modelica.Mechanics.Translational.Sensors.PositionSensor positionSensor annotation(Placement(visible = true, transformation(origin = {2,34}, extent = {{10,10},{-10,-10}}, rotation = 180)));
      Modelica.Blocks.Math.Gain gain1(k = 1) annotation(Placement(visible = true, transformation(origin = {-38,36}, extent = {{10,10},{-10,-10}}, rotation = 180)));
      Components.Reservoir reservoir annotation(Placement(visible = true, transformation(origin = {38,-28}, extent = {{-10,10},{10,-10}}, rotation = 270)));
    equation
      connect(sine2.y,feedback_position.u1) annotation(Line(points = {{-77,0},{-68,0}}, color = {0,0,127}, smooth = Smooth.None));
      connect(cylinder.flange_a,positionSensor.flange) annotation(Line(points = {{30,33},{21,33},{21,34},{12,34}}, color = {0,127,0}, smooth = Smooth.None));
      connect(feedback_position.y,gain3.u) annotation(Line(points = {{-51,0},{-40,0}}, color = {0,0,127}, smooth = Smooth.None));
      connect(gain1.u,positionSensor.s) annotation(Line(points = {{-26,36},{-18,36},{-18,34},{-9,34}}, color = {0,0,127}, smooth = Smooth.None));
      connect(gain1.y,feedback_position.u2) annotation(Line(points = {{-49,36},{-54,36},{-54,8},{-60,8}}, color = {0,0,127}, smooth = Smooth.None));
      connect(const1.y,pressure.signal) annotation(Line(points = {{-17,-34},{9,-34}}, color = {0,0,127}, smooth = Smooth.None));
      connect(pressure.port_B,cylinder.port_A2) annotation(Line(points = {{28,-34},{54,-34},{54,18}}, color = {28,124,126}, smooth = Smooth.None));
      connect(valve33_1.portA,cylinder.port_A1) annotation(Line(points = {{27,10},{36,10},{36,18}}, color = {28,124,126}, smooth = Smooth.None));
      connect(gain3.y,valve33_1.i1) annotation(Line(points = {{-17,0},{16,0}}, color = {0,0,127}, smooth = Smooth.None));
      connect(valve33_1.port_P,pressure.port_B) annotation(Line(points = {{25,-9},{25,-21.5},{28,-21.5},{28,-34}}, color = {28,124,126}, smooth = Smooth.None));
      connect(reservoir.portA,valve33_1.port_T) annotation(Line(points = {{38,-18},{36,-18},{36,-9},{33,-9}}, color = {28,124,126}, smooth = Smooth.None));
      annotation(Placement(transformation(extent = {{30,-16},{80,34}})), Diagram(coordinateSystem(preserveAspectRatio = true, extent = {{-100,-100},{100,100}}), graphics), Diagram(coordinateSystem(preserveAspectRatio = true, extent = {{-100,-100},{100,100}}), graphics));
    end DIRAVI_mission_valve_direct_control;
    annotation(Diagram(coordinateSystem(preserveAspectRatio = true, extent = {{-100,-100},{100,100}}, grid = {1,1}), graphics), Icon(coordinateSystem(preserveAspectRatio = true, extent = {{-100,-100},{100,100}}, grid = {1,1}), graphics = {Rectangle(extent = {{-100,100},{100,-100}}, fillColor = {255,255,255}, fillPattern = FillPattern.Solid, lineColor = {254,254,254}),Ellipse(extent = {{-66,99},{66,-33}}, lineColor = {0,0,0}, fillColor = {255,255,85}, fillPattern = FillPattern.Solid),Rectangle(extent = {{-50,-70},{50,-100}}, lineColor = {0,0,0}, fillColor = {95,95,95}, fillPattern = FillPattern.Solid),Rectangle(extent = {{-40,-40},{40,-70}}, lineColor = {0,0,0}, fillColor = {175,175,175}, fillPattern = FillPattern.Solid),Rectangle(extent = {{-50,-10},{50,-40}}, lineColor = {0,0,0}, fillColor = {95,95,95}, fillPattern = FillPattern.Solid),Ellipse(extent = {{-14,63},{12,38}}, lineColor = {0,0,0}, fillPattern = FillPattern.Solid),Ellipse(extent = {{-24,38},{23,-10}}, lineColor = {0,0,0}, fillPattern = FillPattern.Solid)}));
    package Elevator "Projet ascenseur"
      package Elevator_components "Specific components of elevator example"
        model Car "Sliding mass with inertia and gravity"
          extends Modelica.Mechanics.Translational.Interfaces.PartialRigid;
          parameter SIMM.SIunits.Mass m_Car(min = 0) = 1 "mass of the sliding mass";
          parameter SIMM.SIunits.Acceleration g = 9.81 "gravitational acceleration";
          SIMM.SIunits.Velocity v "absolute velocity of component";
          SIMM.SIunits.Acceleration a "absolute acceleration of component";
        equation
          v = der(s);
          a = der(v);
          m_Car * (a + g) = flange_a.f + flange_b.f;
          annotation(Window(x = 0.23, y = 0.06, width = 0.7, height = 0.63), Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100,-100},{100,100}}, grid = {1,1}), graphics = {Line(points = {{-101,0},{-56,0}}, color = {0,127,0}),Line(points = {{54,0},{99,0}}, color = {0,127,0}),Rectangle(extent = {{-80,50},{81,-50}}, lineColor = {0,0,0}, lineThickness = 1, fillPattern = FillPattern.HorizontalCylinder, fillColor = {135,135,135}),Rectangle(extent = {{-80,49},{81,-51}}, lineColor = {0,0,0}, lineThickness = 1, fillPattern = FillPattern.HorizontalCylinder, fillColor = {135,135,135}),Polygon(points = {{54,-81},{24,-71},{24,-91},{54,-81}}, lineColor = {128,128,128}, fillColor = {128,128,128}, fillPattern = FillPattern.Solid),Line(points = {{-56,-80},{24,-80}}, color = {0,0,0}),Text(extent = {{1,108},{1,64}}, textString = "%name"),Ellipse(extent = {{-49,16},{-18,-14}}, lineColor = {0,127,0}, fillColor = {255,255,255}, fillPattern = FillPattern.Solid),Rectangle(extent = {{-17,18},{31,-16}}, lineColor = {0,127,0}, fillColor = {255,255,255}, fillPattern = FillPattern.Solid),Rectangle(extent = {{33,-1},{75,-12}}, lineColor = {0,127,0}, fillColor = {255,255,255}, fillPattern = FillPattern.Solid),Rectangle(extent = {{-15.5,-18.5},{26,-27}}, lineColor = {0,127,0}, fillColor = {255,255,255}, fillPattern = FillPattern.Solid),Rectangle(extent = {{33,15},{75,4}}, lineColor = {0,127,0}, fillColor = {255,255,255}, fillPattern = FillPattern.Solid),Rectangle(extent = {{-15.5,29.5},{26,21}}, lineColor = {0,127,0}, fillColor = {255,255,255}, fillPattern = FillPattern.Solid)}), Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100,-100},{100,100}}, grid = {1,1}), graphics = {Line(points = {{-100,16},{-55,16}}, color = {0,127,0}),Line(points = {{55,16},{100,16}}, color = {0,127,0}),Rectangle(extent = {{-60,-16},{50,44}}, lineColor = {0,0,0}, fillPattern = FillPattern.Sphere, fillColor = {255,255,255}),Polygon(points = {{50,-90},{20,-80},{20,-100},{50,-90}}, lineColor = {128,128,128}, fillColor = {128,128,128}, fillPattern = FillPattern.Solid),Line(points = {{-60,-90},{20,-90}}, color = {0,0,0}),Line(points = {{-100,-13},{-100,-45}}, color = {0,0,0}),Line(points = {{100,-45},{100,-12}}, color = {0,0,0}),Line(points = {{-98,-44},{98,-44}}, color = {0,0,0}),Polygon(points = {{-101,-44},{-96,-43},{-96,-45},{-101,-44}}, lineColor = {0,0,0}, fillColor = {0,0,0}, fillPattern = FillPattern.Solid),Polygon(points = {{100,-44},{95,-45},{95,-43},{100,-44}}, lineColor = {0,0,0}, fillColor = {0,0,0}, fillPattern = FillPattern.Solid),Text(extent = {{-44,-25},{51,-41}}, textString = "Length L"),Line(points = {{0,46},{0,69}}, color = {0,0,0}),Line(points = {{-72,56},{1,56}}, color = {0,0,0}),Polygon(points = {{-7,58},{-7,54},{-1,56},{-7,58}}, lineColor = {0,0,0}, fillColor = {0,0,0}, fillPattern = FillPattern.Solid),Text(extent = {{-61,69},{-9,58}}, textString = "Position s")}));
        end Car;
        model CounterWeight "Sliding mass with inertia and gravity"
          extends Modelica.Mechanics.Translational.Interfaces.PartialRigid;
          parameter SIMM.SIunits.Mass m_CW(min = 0) = 1 "mass of the sliding mass";
          parameter SIMM.SIunits.Acceleration g = 9.81 "gravitational acceleration";
          SIMM.SIunits.Velocity v "absolute velocity of component";
          SIMM.SIunits.Acceleration a "absolute acceleration of component";
        equation
          v = der(s);
          a = der(v);
          m_CW * (a + g) = flange_a.f + flange_b.f;
          annotation(Window(x = 0.23, y = 0.06, width = 0.7, height = 0.63), Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100,-100},{100,100}}, grid = {1,1}), graphics = {Line(points = {{-100,0},{-55,0}}, color = {0,127,0}),Line(points = {{55,0},{100,0}}, color = {0,127,0}),Rectangle(extent = {{-81,-26},{83,26}}, lineColor = {0,0,0}, fillPattern = FillPattern.Sphere, fillColor = {255,255,255}),Polygon(points = {{59,-60},{29,-50},{29,-70},{59,-60}}, lineColor = {128,128,128}, fillColor = {128,128,128}, fillPattern = FillPattern.Solid),Line(points = {{-51,-60},{29,-60}}, color = {0,0,0}),Text(extent = {{0,100},{0,40}}, textString = "%name")}), Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100,-100},{100,100}}, grid = {1,1}), graphics = {Line(points = {{-100,0},{-55,0}}, color = {0,127,0}),Line(points = {{55,0},{100,0}}, color = {0,127,0}),Rectangle(extent = {{-55,-30},{55,30}}, lineColor = {0,0,0}, fillPattern = FillPattern.Sphere, fillColor = {255,255,255}),Polygon(points = {{50,-90},{20,-80},{20,-100},{50,-90}}, lineColor = {128,128,128}, fillColor = {128,128,128}, fillPattern = FillPattern.Solid),Line(points = {{-60,-90},{20,-90}}, color = {0,0,0}),Line(points = {{-100,-29},{-100,-61}}, color = {0,0,0}),Line(points = {{100,-61},{100,-28}}, color = {0,0,0}),Line(points = {{-98,-60},{98,-60}}, color = {0,0,0}),Polygon(points = {{-101,-60},{-96,-59},{-96,-61},{-101,-60}}, lineColor = {0,0,0}, fillColor = {0,0,0}, fillPattern = FillPattern.Solid),Polygon(points = {{100,-60},{95,-61},{95,-59},{100,-60}}, lineColor = {0,0,0}, fillColor = {0,0,0}, fillPattern = FillPattern.Solid),Text(extent = {{-44,-41},{51,-57}}, textString = "Length L"),Line(points = {{0,30},{0,53}}, color = {0,0,0}),Line(points = {{-72,40},{1,40}}, color = {0,0,0}),Polygon(points = {{-7,42},{-7,38},{-1,40},{-7,42}}, lineColor = {0,0,0}, fillColor = {0,0,0}, fillPattern = FillPattern.Solid),Text(extent = {{-61,53},{-9,42}}, textString = "Position s")}));
        end CounterWeight;
        model Sheave "Pulley with inertia and efficiency"
          import SI = SIMM.SIunits;
          // Definition parameters
          parameter SIMM.SIunits.Length Rsh = 0.35 "Radius of reel";
          parameter SIMM.SIunits.Length L0CarRope = 12 "initial Length of rope connected to the car";
          parameter SI.Efficiency eta = 0.95 "Efficiency";
          parameter SI.MomentOfInertia Jsh = 10 "Moment of inertia";
          // Connectors
          Modelica.Mechanics.Rotational.Interfaces.Flange_a flange_motor annotation(Placement(transformation(extent = {{-107,-37},{-93,-23}}, rotation = 0)));
          Modelica.Mechanics.Translational.Interfaces.Flange_b flange_Car annotation(Placement(transformation(extent = {{37,-80},{43,-86}}, rotation = 0)));
          Modelica.Mechanics.Rotational.Interfaces.Flange_a bearingR annotation(Placement(transformation(extent = {{-59,18},{-44,33}}, rotation = 0)));
          Modelica.Mechanics.Translational.Interfaces.Flange_a bearingT annotation(Placement(transformation(extent = {{45,22},{54,31}}, rotation = 0)));
          Modelica.Mechanics.Translational.Interfaces.Flange_b flange_CWeight annotation(Placement(transformation(extent = {{-43,-80},{-37,-86}}, rotation = 0)));
          // Components
          Pulley pulley(R = Rsh, L0CarRope = L0CarRope) annotation(Placement(transformation(extent = {{-30,-48},{23,7}}, rotation = 0)));
          Modelica.Mechanics.Rotational.Components.Inertia inertia(J = Jsh) annotation(Placement(transformation(extent = {{-90,-40},{-70,-20}}, rotation = 0)));
          Rotational.GearEfficiency friction(eta = eta) annotation(Placement(transformation(extent = {{-63,-40},{-43,-20}}, rotation = 0)));
        equation
          connect(inertia.flange_a,flange_motor) annotation(Line(points = {{-90,-30},{-100,-30}}, color = {0,0,0}));
          connect(pulley.bearingR,bearingR) annotation(Line(points = {{-17.1475,-13.4875},{-17.1475,4.75625},{-51.5,4.75625},{-51.5,25.5}}, color = {0,0,0}));
          connect(pulley.bearingT,bearingT) annotation(Line(points = {{9.6175,-13.2125},{29.8088,-13.2125},{29.8088,26.5},{49.5,26.5}}, color = {0,127,0}));
          connect(flange_CWeight,pulley.flange_CWeight) annotation(Line(points = {{-40,-83},{-27,-83},{-27,-43.325},{-14.1,-43.325}}, color = {0,127,0}));
          connect(flange_Car,pulley.flange_Car) annotation(Line(points = {{40,-83},{40,-63},{7.1,-63},{7.1,-43.325}}, color = {0,127,0}));
          connect(inertia.flange_b,friction.flange_a) annotation(Line(points = {{-70,-30},{-63,-30}}, color = {0,0,0}, pattern = LinePattern.None));
          connect(friction.flange_b,pulley.flange_motor) annotation(Line(points = {{-43,-30},{-37,-30},{-37,-29.3},{-30,-29.3}}, color = {0,0,0}, pattern = LinePattern.None));
          annotation(Window(x = 0.35, y = 0.1, width = 0.6, height = 0.65), Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100,-100},{100,100}}, grid = {1,1}), graphics = {Rectangle(extent = {{-100,-23},{-41,-37}}, lineColor = {0,0,0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {192,192,192}),Ellipse(extent = {{-41,8},{40,-71}}, lineColor = {0,0,0}, fillColor = {95,95,95}, fillPattern = FillPattern.Solid),Rectangle(extent = {{44,32},{56,20}}, lineColor = {192,192,192}, fillColor = {192,192,192}, fillPattern = FillPattern.Solid),Ellipse(extent = {{-31,-2},{28,-60}}, lineColor = {0,0,0}, fillColor = {255,255,255}, fillPattern = FillPattern.Solid),Text(extent = {{-77,57},{67,37}}, textString = "%name=%R"),Rectangle(extent = {{38,-29},{41,-81}}, lineColor = {0,0,0}, fillColor = {95,95,95}, fillPattern = FillPattern.Solid),Rectangle(extent = {{-42,-30},{-39,-80}}, lineColor = {0,0,0}, fillColor = {95,95,95}, fillPattern = FillPattern.Solid),Polygon(points = {{-52,26},{49,26},{10,-31},{-13,-31},{-52,26}}, lineColor = {0,0,255}, fillColor = {0,17,255}, fillPattern = FillPattern.Solid),Ellipse(extent = {{-13,-17},{10,-41}}, lineColor = {0,0,255}, fillColor = {0,17,255}, fillPattern = FillPattern.Solid),Ellipse(extent = {{-9,-23},{6,-39}}, lineColor = {0,0,0}, fillPattern = FillPattern.Sphere, fillColor = {0,0,0})}), Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100,-100},{100,100}}, grid = {1,1}), graphics));
        end Sheave;
        model Pulley
          parameter SIMM.SIunits.Length R = 0.2 "Radius of reel";
          parameter SIMM.SIunits.Length L0CarRope = 12 "initial Length of rope connected to the car";
          SIMM.SIunits.Length L_CarRope "rest L_CarRope of unwound cable";
          SIMM.SIunits.Torque tau_support;
          SIMM.SIunits.Torque tau_total "total torque that provides traction";
          SIMM.SIunits.Force f_support;
          Modelica.Mechanics.Rotational.Interfaces.Flange_a flange_motor annotation(Placement(transformation(extent = {{-107,-39},{-93,-25}}, rotation = 0)));
          Modelica.Mechanics.Translational.Interfaces.Flange_b flange_Car annotation(Placement(transformation(extent = {{37,-80},{43,-86}}, rotation = 0)));
          Modelica.Mechanics.Rotational.Interfaces.Flange_a bearingR annotation(Placement(transformation(extent = {{-59,18},{-44,33}}, rotation = 0)));
          Modelica.Mechanics.Translational.Interfaces.Flange_a bearingT annotation(Placement(transformation(extent = {{45,22},{54,31}}, rotation = 0)));
          Modelica.Mechanics.Translational.Interfaces.Flange_b flange_CWeight annotation(Placement(transformation(extent = {{-43,-80},{-37,-86}}, rotation = 0)));
          //initial equation
          //  L_CarRope = L0CarRope;
        equation
          // make sure the L_CarRope never goes to zero because that would
          // result in a singularity
          assert(L_CarRope > 0, "Length of rope on Car must be positive");
          // Car's displacement = - (CW's displacement)
          flange_Car.s + flange_CWeight.s = 0;
          // Rope length
          L_CarRope = bearingT.s - flange_Car.s + L0CarRope;
          tau_total = flange_motor.tau;
          // relate the angle to the length
          flange_motor.phi - bearingR.phi = -(bearingT.s - flange_Car.s) / R;
          // force and torque balance
          0 = tau_total / R + flange_Car.f - flange_CWeight.f;
          0 = tau_total + tau_support;
          0 = flange_Car.f + flange_CWeight.f + f_support;
          if cardinality(bearingR) == 0 then
            bearingR.phi = 0;
          else
            bearingR.tau = tau_support;
          end if;
          if cardinality(bearingT) == 0 then
            bearingT.s = 0;
          else
            bearingT.f = f_support;
          end if;
          annotation(Window(x = 0.35, y = 0.1, width = 0.6, height = 0.65), Icon(coordinateSystem(preserveAspectRatio = false, extent = {{-100,-100},{100,100}}, grid = {1,1}), graphics = {Rectangle(extent = {{-100,-25},{-41,-39}}, lineColor = {0,0,0}, fillPattern = FillPattern.HorizontalCylinder, fillColor = {192,192,192}),Ellipse(extent = {{-41,8},{40,-71}}, lineColor = {0,0,0}, fillColor = {95,95,95}, fillPattern = FillPattern.Solid),Rectangle(extent = {{44,32},{56,20}}, lineColor = {192,192,192}, fillColor = {192,192,192}, fillPattern = FillPattern.Solid),Ellipse(extent = {{-31,-2},{28,-60}}, lineColor = {0,0,0}, fillColor = {255,255,255}, fillPattern = FillPattern.Solid),Text(extent = {{-77,57},{67,37}}, textString = "%name=%R"),Rectangle(extent = {{38,-29},{41,-81}}, lineColor = {0,0,0}, fillColor = {95,95,95}, fillPattern = FillPattern.Solid),Rectangle(extent = {{-42,-30},{-39,-80}}, lineColor = {0,0,0}, fillColor = {95,95,95}, fillPattern = FillPattern.Solid),Polygon(points = {{-52,26},{49,26},{10,-31},{-13,-31},{-52,26}}, lineColor = {0,0,255}, fillColor = {0,17,255}, fillPattern = FillPattern.Solid),Ellipse(extent = {{-13,-17},{10,-41}}, lineColor = {0,0,255}, fillColor = {0,17,255}, fillPattern = FillPattern.Solid),Ellipse(extent = {{-9,-23},{6,-39}}, lineColor = {0,0,0}, fillPattern = FillPattern.Sphere, fillColor = {0,0,0})}), Diagram(coordinateSystem(preserveAspectRatio = false, extent = {{-100,-100},{100,100}}, grid = {1,1}), graphics = {Text(extent = {{-100,70},{100,40}}, textString = "transform rotation into translation"),Polygon(points = {{16,80},{-4,85},{-4,75},{16,80}}, lineColor = {128,128,128}, fillColor = {128,128,128}, fillPattern = FillPattern.Solid),Line(points = {{-74,80},{-3,80}}, color = {128,128,128}),Text(extent = {{21,88},{89,75}}, lineColor = {128,128,128}, textString = "rotation axis")}));
        end Pulley;
      end Elevator_components;
      model Elevator_mission_hydraulic
        MissionProfil.Profil.PathPTP_Jmax pathPTP_Jmax(N = 4, StartTime = {0,15,30,45}, Qd_max = 2, Deltaq = {12,-12,-8,8}, Qd_init = 0, Qdd_max = 1, Qddd_max = 2.5) annotation(Placement(transformation(extent = {{-60,-100},{-40,-80}}, rotation = 0)));
      public
        Modelica.Blocks.Continuous.SecondOrder Angle_Filter1(w = 500) annotation(Placement(transformation(extent = {{-9,-100},{11,-80}}, rotation = 0)));
      public
        Elevator_components.Car car(m_Car = 1000) annotation(Placement(transformation(origin = {68,16}, extent = {{-12,-10},{12,10}}, rotation = 270)));
        Modelica.Mechanics.Translational.Sensors.PowerSensor powerSensor annotation(Placement(transformation(extent = {{-10,-10},{10,10}}, rotation = 90, origin = {68,-12})));
        Modelica.Mechanics.Translational.Sources.Position position annotation(Placement(transformation(extent = {{-10,-10},{10,10}}, rotation = 90, origin = {68,-50})));
      equation
        connect(pathPTP_Jmax.y,Angle_Filter1.u) annotation(Line(points = {{-39,-84},{-34,-84},{-34,-90},{-11,-90}}, color = {0,0,127}));
        connect(powerSensor.flange_b,car.flange_b) annotation(Line(points = {{68,-2},{68,4}}, color = {0,127,0}, smooth = Smooth.None));
        connect(position.flange,powerSensor.flange_a) annotation(Line(points = {{68,-40},{68,-22}}, color = {0,127,0}, smooth = Smooth.None));
        connect(position.s_ref,Angle_Filter1.y) annotation(Line(points = {{68,-62},{68,-90},{12,-90}}, color = {0,0,127}, smooth = Smooth.None));
        annotation(Diagram(coordinateSystem(preserveAspectRatio = true, extent = {{-100,-100},{100,100}}), graphics));
      end Elevator_mission_hydraulic;
    end Elevator;
  end Examples;
