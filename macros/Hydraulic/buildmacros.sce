
    // This file is released under the 3-clause BSD license. See COPYING-BSD.

function buildmacros()
  macros_path = get_absolute_file_path("buildmacros.sce");
  tbx_build_macros(TOOLBOX_NAME+"_H_", macros_path);
endfunction

function buildblocks()
  macros_path = get_absolute_file_path("buildmacros.sce");
  sources_blocks = ["FlowVolumetric"
                    "Pressure"
                   ]
  basic_blocks = [
                "SimpleCylinder"
                "Cylinder"
                  "Tank"
                  "PressureForceTranslator"
                  "Pump"
                  "PumpCylinderVariable"
                 "Valve33"
                  "Valve43"
                  "Valve22"
                  "Valve53"
                  "CheckValve"
                 ]
  parts_blocks = [
  "Laminar"
  "Turbulent"
  "TurbulentControled"
                  "RotComp"
                  "ChamberLeft"
                  "ChamberRight"                  
                  "VolEfficiency"
                  "HydraulicCapacity"
                 ]
                  
  sensors_blocks=["FlowVolumetricSensor"
                    "PressureSensor"] 
  blocks = [sources_blocks
            sensors_blocks
            basic_blocks
            parts_blocks
           ]
           
  tbx_build_blocks(toolbox_dir, blocks, macros_path);
endfunction


buildmacros();
getd(get_absolute_file_path("buildmacros.sce"));
buildblocks();
clear buildmacros; // remove buildmacros on stack
