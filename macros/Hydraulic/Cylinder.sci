//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012-2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
function [x,y,typ]=Cylinder(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'set' then
        x=arg1;
        graphics=x.graphics;
        exprs=graphics.exprs;
        model=x.model;

        while %t do
            [ok,Stroke,Mass,LeftArea,RightArea,exprs]=...
              getvalue([__('Hydraulic - Cylinder');__('Double acting cylinder')],...
                       [__('Stroke (m)');...
                        __('Mass of rod (kg)');...
                        __('Effective area of left side of piston (m)');...
                        __('Effective area of right side of piston (m)')],...
                       list('vec',1,'vec',1,'vec',1,'vec',1),exprs);
            
            if ~ok then
                break
            end

            if ok then
                model.equations.parameters(2)=list(Stroke,Mass,LeftArea,RightArea)
                graphics.exprs=exprs;
                x.graphics=graphics;x.model=model;
                break
            end
        end

    case 'define' then
      model=scicos_model();
      //parametres par defaut
      Stroke=0.05
      Mass=100;
      LeftArea=0.005;
      RightArea=0.005;
      //modele modelica
      model.sim='SIMM';
      model.blocktype='c';
      model.dep_ut=[%t %f];
      mo=modelica();
      mo.model='Hydraulic.Components.Cylinder';
      mo.inputs=['flange_a','port_A1'];
      mo.outputs=['flange_b','port_A2'];
      mo.parameters=list(['Stroke','MobileMass','PistonAreaLeft','PistonAreaRight'],...
                         list(Stroke,Mass,LeftArea,RightArea),...
                         [0,0,0,0]);
      model.equations=mo;
      model.in=ones(size(mo.inputs,'*'),1);
      model.out=ones(size(mo.outputs,'*'),1);
      exprs=string([Stroke;Mass;LeftArea;RightArea]);
      gr_i=[];
      x=standard_define([2 2],model,exprs,list(gr_i,0));
      x.graphics.in_implicit=['I','I'];
      x.graphics.in_style=[TransInputStyle(),HydraulicInputStyle()];
      x.graphics.out_implicit=['I','I'];
      x.graphics.out_style=[TransOutputStyle(),HydraulicOutputStyle()];
    end
endfunction
