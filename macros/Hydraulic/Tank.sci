//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012-2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
function [x,y,typ]=Tank(job,arg1,arg2)
     x=[];y=[];typ=[];
    select job
    case 'set' then
        x=arg1;
        graphics=x.graphics;
        exprs=graphics.exprs;
        model=x.model;
        while %t do

            [ok,ppreload,exprs] = getvalue([__('Hydraulic - Tank');__('Tank with preload but without resistance')],..
            [__('Pressure preload (Pa)')],..
            list('vec',1),exprs);
            if ~ok then
                break
            end

            if ok then
                model.equations.parameters(2)=list(ppreload)
                graphics.exprs=exprs;
                x.graphics=graphics;x.model=model;
                break
            end
        end

    case 'define' then
      model=scicos_model();
      //parametres par defaut
      ppreload=0.0
      //modele modelica
      model.sim='SIMM';
      model.blocktype='c';
      model.dep_ut=[%t %f];
      mo=modelica();
      mo.model='Hydraulic.Components.Reservoir';
      mo.inputs=['portA'];
      mo.outputs=[];
      mo.parameters=list(['ppreload'],...
                         list(ppreload),...
                         [0]);
      model.equations=mo;
      model.in=ones(size(mo.inputs,'*'),1);
      model.out=ones(size(mo.outputs,'*'),1);
      exprs=sci2exp([ppreload]);
      gr_i=[];
      x=standard_define([2 2],model,exprs,list(gr_i,0));
      x.graphics.in_implicit=['I'];
      x.graphics.in_style=[HydraulicInputStyle()];
      x.graphics.out_implicit=[];
    end
endfunction
