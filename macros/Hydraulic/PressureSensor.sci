//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012-2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//

function [x,y,typ]=PressureSensor(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'set' then
        x=arg1;

    case 'define' then
        model=scicos_model();
        model.sim='SIMM';
        model.blocktype='c';
        model.dep_ut=[%t %f];
        mo=modelica();
        mo.model='Hydraulic.Components.Sensors.Pressure';
        mo.inputs=['port_A'];
        mo.outputs=['p'];
        model.equations=mo;
        model.in=ones(size(mo.inputs,'*'),1);
        model.out=ones(size(mo.outputs,'*'),1);
        exprs=[];
        x=standard_define([2 2],model,exprs,list([],0));
        x.graphics.in_implicit=['I'];
        x.graphics.in_style=[HydraulicInputStyle()];
        x.graphics.out_implicit=['I'];
        x.graphics.out_style=[RealOutputStyle()];
    end
endfunction
