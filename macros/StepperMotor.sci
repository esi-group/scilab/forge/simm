// Coselica Toolbox for Xcos
// modif Thierry ROYANT 2014   Diode d'un panneau solaire suite modifs 5 2015
// Copyright (C) 2011 - DIGITEO - Bruno JOFRET
// Copyright (C) 2009, 2010  Dirk Reusch, Kybernetik Dr. Reusch
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

function [x,y,typ]=StepperMotor(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
     case 'set' then
      x=arg1;
      graphics=arg1.graphics;exprs=graphics.exprs;
      model=arg1.model;
      while %t do
          [ok,R,L,k,np,J,fv,exprs]=...
              getvalue(['StepperMotor';__('Moteur pas à pas')],...
                       [__('R [Ohm]: Résistance statorique');...
                        __('L [H] : Inductance statorique');...
                        __('k [Nm/A]]: fcem');...
                        __('np : nombre de paire de poles (=nombre de pas par tours/4)');...
                        __('J [kg.m^2]: moment d''inertier du rotor');...
                        __('fv [N.s/rad]: frottement visqueux')],...
                       list('vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1),exprs);
          if ~ok then break, end
          model.equations.parameters(2)=list(R,L,k,np,J,fv)

          graphics.exprs=exprs;
          x.graphics=graphics;x.model=model;
          break
      end
     case 'define' then
      model=scicos_model();
      R=1;
      L=0.001;
      k=0.02;
      np=50;
      J=0.000001;
      fv=0.01;
      model.sim='SIMM';
      model.blocktype='c';
      model.dep_ut=[%t %f];
      mo=modelica();
      mo.model='SIMM.StepperMotor';
      mo.inputs=['pA','nA','pB','nB'];
      mo.outputs=['rot'];
      mo.parameters=list(['R','L','k','np','J','fv'],...
                         list(R,L,k,np,J,fv),...
                         [0,0,0,0,0,0]);
      model.equations=mo;
      model.in=ones(size(mo.inputs,'*'),1);
      model.out=ones(size(mo.outputs,'*'),1);
      exprs=string([R;L;k;np;J;fv]);
      gr_i=[];
      x=standard_define([5 5],model,exprs,list(gr_i,0));
      x.graphics.in_implicit=['I','I','I','I'];
      x.graphics.in_style=[ElecInputStyle(),ElecOutputStyle(),ElecInputStyle(),ElecOutputStyle()];
      x.graphics.out_implicit=['I'];
      x.graphics.out_style=[RotOutputStyle()];
          in_label=["pA";"nA";"pB";"nB"]
      x.graphics.in_label=in_label;
    end
endfunction
