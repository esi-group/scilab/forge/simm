//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012-2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
function [x,y,typ]=AnimBar2D(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'set' then
	   x=arg1
//       graphics=arg1.graphics;
//        exprs=graphics.exprs
//        model=arg1.model;
//        
//        //Choix du type de diagramme
//        while (%t)  do
//            [ok,xmin,xmax,ymin,ymax,segthickness,echantillonnage,nb_bars,exprs]=scicos_getvalue('AnimBar2D parameters',..
//                                                        [gettext('Xmin');gettext('Xmax');gettext('Ymin');gettext('Ymax');gettext('Epaisseur des traits');gettext('Echantillonnage (s)');gettext('Nombre de barres')], ..
//                                                        list('vec',1,'vec',1,'vec',1,'vec', 1,'vec',1,'vec',1,'vec',1), ..
//                                                        exprs)
//          mess=[];
//
//          if ~ok then // Cancel
//              break;
//          end
//
//          if xmax <= xmin
//              mess=[mess ;"Xmin doit être plus petit que Xmax"]
//              ok = %f
//          end
//          if ymax <= ymin
//              mess=[mess ;"Ymin doit être plus petit que Ymax"]
//              ok = %f
//          end
//
//          if echantillonnage<=0
//              mess=[mess ;"L''échantillonnage doit être positif"]
//              ok = %f
//          end
//
//          if segthickness<=0
//              mess=[mess ;"La taille des traits doit être positive"]
//              ok = %f
//          end
//
//          if ok then // Everything's ok
//              graphics.exprs =  exprs;
//              input2=string(ones(nb_bars*2,1))
//              graphics.in_implicit=strsubst(input2,'1','I');
//              
//              diagram=animate_bars_xy(nb_bars,exprs(1:6))
//              model.rpar=diagram
//              model.in=-1*ones(nb_bars*2,1)
//              model.in2=-2*ones(nb_bars*2,1)
//              model.intyp=-1*ones(nb_bars*2,1)
//              
//              
//              x.graphics = graphics;
//              x.model=model
//              break
//          else
//              message(mess);
//          end
//        end
    case 'define' then  
        nb_bars=1
        parameters=string([-15;15;-15;15;3;0.1])
        diagram=animate_bars_xy(nb_bars,parameters)
        
        model=scicos_model();
        model.sim='csuper'
        model.blocktype='h';
        model.dep_ut=[%f %f];
        model.rpar=diagram
        model.in=-1*ones(nb_bars*2,1)
        model.in2=-2*ones(nb_bars*2,1)
        model.intyp=-1*ones(nb_bars*2,1)
        model.out=[]
        model.out2=[]

        x=standard_define([2 2],model,[],[]);
        input2=string(ones(nb_bars*2,1))
        x.graphics.in_implicit=strsubst(input2,'1','I');
        x.graphics.style=["blockWithLabel;verticalLabelPosition=center;displayedLabel=AnimBar"]
        x.graphics.exprs=[parameters;string(nb_bars)};
    end
endfunction
