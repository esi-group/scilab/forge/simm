//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012-2013 - David Violeau
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
function [x,y,typ]=CEAS_PredefCurrent(job,arg1,arg2)
    x=[];y=[];typ=[];
    select job
    case 'plot' then
        //      standard_draw(arg1,%f,_CMTS_PositionSensor_dp);
    case 'getinputs' then
        //      [x,y,typ]=_CMTS_PositionSensor_ip(arg1);
    case 'getoutputs' then
        //      [x,y,typ]=_CMTS_PositionSensor_op(arg1);
    case 'getorigin' then
        //      [x,y]=standard_origin(arg1);
    case 'set' then
        x=arg1;
        graphics=x.graphics;
        //exprs=graphics.exprs;
        model=x.model;
        while %t do

            [ok,Value,exprs] = getvalue(['Predefined Source Current'],..
            ['Type of signal : (0) constant, (1) step, (2) ramp, (3) sine, (4) pulse, (5) sawtooth, (6) trapezoid'],..
            list('vec',1),graphics.exprs(1));
            if ~ok then
                break
            end
            if Value <0 | Value >7 then
                ok = %f
                message("Choose type of signal between 0 and 6")
            end
            if ok then
                select Value
                case 0 //constant
                    ok2=%f
                    if graphics.exprs(1)==sci2exp(Value) then //set
                        exprs2=graphics.exprs(2:$);
                    else //define
                        I=1;
                        exprs2=[sci2exp(I)];
                    end
                    while %t do
                        [ok2,I,exprs2]=...
                        getvalue(['';'MEAS_ConstantCurrent';'';'Source for constant Current';''],...
                        [' I [A] : Value of constant Current'],...
                        list('vec',1),exprs2);
                        if ~ok2 then break, end
                        mo=modelica();
                        mo.model='Modelica.Electrical.Analog.Sources.ConstantCurrent';
                        mo.parameters=list(['I'],...
                        list(I),...
                        [0]);
                        mo.inputs=['p'];
                        mo.outputs=['n'];
                        model.in=ones(size(mo.inputs,'*'),1);
                        model.out=ones(size(mo.outputs,'*'),1);                       
                        model.equations=mo;
                        model.equations.parameters(2)=list(I)
                        graphics.exprs=[exprs;exprs2];
                        x=standard_define([2, 2],model,graphics.exprs,list([], 0));
                        x.graphics.in_implicit=['I'];
                        x.graphics.in_style=[ElecInputStyle()];
                        x.graphics.out_implicit=['I'];
                        x.graphics.out_style=[ElecOutputStyle()];
                        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Const.<br><br><br>"]
                        break
                    end

                case 1 //step
                    ok2=%f
                    if graphics.exprs(1)==sci2exp(Value) then //set
                        exprs2=graphics.exprs(2:$);
                    else //define
                        I=1;
                        offset=0;
                        startTime=0;
                        exprs2=[sci2exp(I);sci2exp(offset);sci2exp(startTime)]
                    end
                    while %t do
                        [ok2,I,offset,startTime,exprs2]=...
                        getvalue(['';'MEAS_StepCurrent';'';'Step Current source';''],...
                        [' I [A] : Height of step';' offset [A] : Current offset';' startTime [s] : Time offset'],...
                        list('vec',1,'vec',1,'vec',1),exprs2);

                        if ~ok2 then break, end
                        mo=modelica();
                        mo.model='Modelica.Electrical.Analog.Sources.StepCurrent';
                        mo.parameters=list(['I','offset','startTime'],...
                        list(I,offset,startTime),...
                        [0,0,0]);
                        mo.inputs=['p'];
                        mo.outputs=['n'];
                        model.in=ones(size(mo.inputs,'*'),1);
                        model.out=ones(size(mo.outputs,'*'),1);                       
                        model.equations=mo;
                        model.equations.parameters(2)=list(I,offset,startTime)
                        graphics.exprs=[exprs;exprs2];
                        x=standard_define([2, 2],model,graphics.exprs,list([], 0));
                        x.graphics.in_implicit=['I'];
                        x.graphics.in_style=[ElecInputStyle()];
                        x.graphics.out_implicit=['I'];
                        x.graphics.out_style=[ElecOutputStyle()];
                        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Step<br><br>"]
                        break
                    end
                case 2 //ramp
                    ok2=%f
                    if graphics.exprs(1)==sci2exp(Value) then //set
                        exprs2=graphics.exprs(2:$);
                    else //define
                        I=1;
                        duration=2;
                        offset=0;
                        startTime=0;
                        exprs2=[sci2exp(I);sci2exp(duration);sci2exp(offset);sci2exp(startTime)];
                    end
                    while %t do
                        [ok2,I,duration,offset,startTime,exprs2]=...
                        getvalue(['';'MEAS_RampCurrent';'';'Ramp Current source';''],...
                        [' I [A] : Height of ramp';' duration [s] : Duration of ramp';' offset [A] : Current offset';' startTime [s] : Time offset'],...
                        list('vec',1,'vec',1,'vec',1,'vec',1),exprs2);


                        if ~ok2 then break, end
                        mo=modelica();
                        mo.model='Modelica.Electrical.Analog.Sources.RampCurrent';
                        mo.parameters=list(['I','duration','offset','startTime'],...
                        list(I,duration,offset,startTime),...
                        [0,0,0,0]);                        
                        mo.inputs=['p'];
                        mo.outputs=['n'];
                        model.in=ones(size(mo.inputs,'*'),1);
                        model.out=ones(size(mo.outputs,'*'),1);                       
                        model.equations=mo;
                        model.equations.parameters(2)=list(I,offset,startTime)
                        model.equations.parameters(2)=list(I,duration,offset,startTime)
                        graphics.exprs=[exprs;exprs2];
                        x=standard_define([2, 2],model,graphics.exprs,list([], 0));
                        x.graphics.in_implicit=['I'];
                        x.graphics.in_style=[ElecInputStyle()];
                        x.graphics.out_implicit=['I'];
                        x.graphics.out_style=[ElecOutputStyle()];
                        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Ramp<br><br>"]
                        break
                    end
                case 3 //sine
                    ok2=%f
                    if graphics.exprs(1)==sci2exp(Value) then //set
                        exprs2=graphics.exprs(2:$);
                    else //define
                        I=1;
                        phase=0;
                        freqHz=1;
                        offset=0;
                        startTime=0;
                        exprs2=[sci2exp(I);sci2exp(phase);sci2exp(freqHz);sci2exp(offset);sci2exp(startTime)];
                    end
                    while %t do
                        [ok2,I,phase,freqHz,offset,startTime,exprs2]=...
                        getvalue(['';'MEAS_SineCurrent';'';'Sine Current source';''],...
                        [' I [A] : Amplitude of sine waIe';' phase [rad] : Phase of sine waIe';' freqHz [Hz] : Frequency of sine waIe';' offset [A] : Current offset';' startTime [s] : Time offset'],...
                        list('vec',1,'vec',1,'vec',1,'vec',1,'vec',1),exprs2);

                        if ~ok2 then break, end
                        mo=modelica();
                        mo.model='Modelica.Electrical.Analog.Sources.SineCurrent';
                        mo.parameters=list(['I','phase','freqHz','offset','startTime'],...
                        list(I,phase,freqHz,offset,startTime),...
                        [0,0,0,0,0]);
                        mo.inputs=['p'];
                        mo.outputs=['n'];
                        model.in=ones(size(mo.inputs,'*'),1);
                        model.out=ones(size(mo.outputs,'*'),1);                       
                        model.equations=mo;
                        model.equations.parameters(2)=list(I,phase,freqHz,offset,startTime)
                        graphics.exprs=[exprs;exprs2];
                        x=standard_define([2, 2],model,graphics.exprs,list([], 0));
                        x.graphics.in_implicit=['I'];
                        x.graphics.in_style=[ElecInputStyle()];
                        x.graphics.out_implicit=['I'];
                        x.graphics.out_style=[ElecOutputStyle()];
                        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Sin.<br><br>"]
                        break
                    end
                case 4 //pulse
                    ok2=%f
                    if graphics.exprs(1)==sci2exp(Value) then //set
                        exprs2=graphics.exprs(2:$);
                    else //define
                        I=1;
                        width=50;
                        period=1;
                        offset=0;
                        startTime=0;
                        exprs2=[sci2exp(I);sci2exp(width);sci2exp(period);sci2exp(offset);sci2exp(startTime)];
                    end
                    while %t do
                        [ok2,I,width,period,offset,startTime,exprs2]=...
                        getvalue(['';'MEAS_PulseCurrent';'';'Pulse Current source';''],...
                        [' I [A] : Amplitude of pulse';' width [-] : Width of pulse in % of period';' period [s] : Time for one period';' offset [A] : Current offset';' startTime [s] : Time offset'],...
                        list('vec',1,'vec',1,'vec',1,'vec',1,'vec',1),exprs2);

                        if ~ok2 then break, end
                        mo=modelica();
                        mo.model='Modelica.Electrical.Analog.Sources.PulseCurrent';
                        mo.parameters=list(['I','width','period','offset','startTime'],...
                        list(I,width,period,offset,startTime),...
                        [0,0,0,0,0]);
                        mo.inputs=['p'];
                        mo.outputs=['n'];
                        model.in=ones(size(mo.inputs,'*'),1);
                        model.out=ones(size(mo.outputs,'*'),1);                       
                        model.equations=mo;
                        model.equations.parameters(2)=list(I,width,period,offset,startTime)
                        graphics.exprs=[exprs;exprs2];
                        x=standard_define([2, 2],model,graphics.exprs,list([], 0));
                        x.graphics.in_implicit=['I'];
                        x.graphics.in_style=[ElecInputStyle()];
                        x.graphics.out_implicit=['I'];
                        x.graphics.out_style=[ElecOutputStyle()];
                        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Pulse<br><br>"]
                        break
                    end
                case 5 //sawtooth
                    ok2=%f
                    if graphics.exprs(1)==sci2exp(Value) then //set
                        exprs2=graphics.exprs(2:$);
                    else //define
                        I=1;
                        period=1;
                        offset=0;
                        startTime=0;
                        exprs2=[sci2exp(I);sci2exp(period);sci2exp(offset);sci2exp(startTime)];
                    end
                    while %t do
                        [ok2,I,period,offset,startTime,exprs2]=...
                        getvalue(['';'MEAS_SawToothCurrent';'';'Saw tooth Current source';''],...
                        [' I [A] : Amplitude of saw tooth';' period [s] : Time for one period';' offset [A] : Current offset';' startTime [s] : Time offset'],...
                        list('vec',1,'vec',1,'vec',1,'vec',1),exprs2);

                        if ~ok2 then break, end
                        mo=modelica();
                        mo.model='Modelica.Electrical.Analog.Sources.SawToothCurrent';
                        mo.parameters=list(['I','period','offset','startTime'],...
                        list(I,period,offset,startTime),...
                        [0,0,0,0]);
                        mo.inputs=['p'];
                        mo.outputs=['n'];
                        model.in=ones(size(mo.inputs,'*'),1);
                        model.out=ones(size(mo.outputs,'*'),1);                       
                        model.equations=mo;
                        model.equations.parameters(2)=list(I,period,offset,startTime)
                        graphics.exprs=[exprs;exprs2];
                        x=standard_define([2, 2],model,graphics.exprs,list([], 0));
                        x.graphics.in_implicit=['I'];
                        x.graphics.in_style=[ElecInputStyle()];
                        x.graphics.out_implicit=['I'];
                        x.graphics.out_style=[ElecOutputStyle()];
                        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Saw.<br><br>"]
                        break
                    end
                case 6 //trapezoid
                    ok2=%f
                    if graphics.exprs(1)==sci2exp(Value) then //set
                        exprs2=graphics.exprs(2:$);
                    else //define
                        I=1;
                        rising=0;
                        width=0.5;
                        falling=0;
                        period=1;
                        nperiod=-1;
                        offset=0;
                        startTime=0;
                        exprs2=[sci2exp(I);sci2exp(rising);sci2exp(width);sci2exp(falling);sci2exp(period);sci2exp(nperiod);sci2exp(offset);sci2exp(startTime)];
                    end
                    while %t do
                        [ok2,I,rising,width,falling,period,nperiod,offset,startTime,exprs2]=...
                        getvalue(['';'CEAS_TrapezoidCurrent';'';'Trapezoidal Current source';''],...
                        [' I [A] : Amplitude of trapezoid';' rising [s] : Rising duration of trapezoid (>=0)';' width [s] : Width duration of trapezoid (>=0)';' falling [s] : Falling duration of trapezoid (>=0)';' period [s] : Time for one period (>0)';' nperiod [-] : Number of periods (< 0 means infinite number of periods)';' offset [A] : Current offset';' startTime [s] : Time offset'],...
                        list('vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1,'vec',1),exprs2);

                        if ~ok2 then break, end
                        mo=modelica();
                        mo.model='Coselica.Electrical.Analog.Sources.TrapezoidCurrent';
                        mo.parameters=list(['I','rising','width','falling','period','nperiod','offset','startTime'],...
                        list(I,rising,width,falling,period,nperiod,offset,startTime),...
                        [0,0,0,0,0,0,0,0]);
                        mo.inputs=['p'];
                        mo.outputs=['n'];
                        model.in=ones(size(mo.inputs,'*'),1);
                        model.out=ones(size(mo.outputs,'*'),1);                       
                        model.equations=mo;
                        model.equations.parameters(2)=list(I,rising,width,falling,period,nperiod,offset,startTime)
                        graphics.exprs=[exprs;exprs2];
                        x=standard_define([2, 2],model,graphics.exprs,list([], 0));
                        x.graphics.in_implicit=['I'];
                        x.graphics.in_style=[ElecInputStyle()];
                        x.graphics.out_implicit=['I'];
                        x.graphics.out_style=[ElecOutputStyle()];
                        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Trapez.<br><br>"]
                        break
                    end
                end
                break
            end
        end

    case 'define' then
        model=scicos_model();
        model.sim='Coselica';
        model.blocktype='c';
        model.dep_ut=[%t %f];
        model.in=[1];
        model.out=[1];
        I=1;
        mo=modelica();
        mo.model='Modelica.Electrical.Analog.Sources.ConstantCurrent';
        mo.inputs=['p'];
        mo.outputs=['n'];
        mo.parameters=list(['I'],...
        list(I),...
        [0]);
        model.equations=mo;
        model.in=ones(size(mo.inputs,'*'),1);
        model.out=ones(size(mo.outputs,'*'),1);
        exprs=['0',sci2exp(I)];
        x=standard_define([2, 2],model,exprs,list([], 0));
        x.graphics.in_implicit=['I'];
        x.graphics.in_style=[ElecInputStyle()];
        x.graphics.out_implicit=['I'];
        x.graphics.out_style=[ElecOutputStyle()];
        x.graphics.style=["blockWithLabel;verticalLabelPosition=middle;verticalAlign=top;spacing=-2;displayedLabel=Const.<br><br><br>"]
    end
endfunction
