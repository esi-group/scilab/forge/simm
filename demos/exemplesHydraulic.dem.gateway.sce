// Copyright (C) 2013 - Scilab Enterprises - Bruno JOFRET
//
// This file is released under the 3-clause BSD license. See COPYING-BSD.

function subdemolist = demo_gateway()

  demopath = get_absolute_file_path("exemplesHydraulic.dem.gateway.sce");
  subdemolist = ["pompe_test1", "pompe_test1.dem.sce" ;
                 "pompe_test2", "pompe_test2.dem.sce" ;
                 "valve33_test", "valve33_test.dem.sce" ;
                 "valve43_test", "valve43_test.dem.sce" ;
                 "valve53_test", "valve53_test.dem.sce" ;
                 "verin_simple_effet_test", "verin_simple_effet_test.dem.sce" ;
                 "verin_double_effet_test", "verin_simple_effet_test.dem.sce" ;
                ]; // add demos here
  subdemolist(:,2) = demopath + subdemolist(:,2);

endfunction

subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
