//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2010-2010 - DIGITEO - Bruno JOFRET
// Copyright (C) 2012-2012 - Scilab Enterprises - Bruno JOFRET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
function subdemolist = demo_gateway()
  demopath = get_absolute_file_path("simm.dem.gateway.sce");

  subdemolist = ["Hacheur 1 quadrant - Circuit RL"                   , "Q1driver_RL.dem.sce";
                 "Moteur à courant continu avec frottement visqueux" , "MoteurCC.dem.sce";
                 "Régulation de température de pièce"                , "Regulation_temperature.dem.sce";
                 "Angiographe"                                       , "Angiographe.dem.sce";
                 "Exemples Livret"                                   , "exemplesLivret.dem.gateway.sce";
                 "Exemples hydrauliques"                             , "exemplesHydraulic.dem.gateway.sce";
                ];

  subdemolist(:,2) = demopath + subdemolist(:,2);

endfunction

subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
