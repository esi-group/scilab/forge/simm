// This file is released under the 3-clause BSD license. See COPYING-BSD.
function runMe()
    toolbox_dir = get_absolute_file_path("electrical.sce")+"..";
    subdir=[ 'SIMM', 'Electrique']

    name_subpal= _("Sources")

    cos_blocks = ["MEAS_SignalVoltage"
                  "MEAS_SignalCurrent"
                  "MEAB_Ground"
                 ]
    blocks=[
        "CEAS_PredefVoltage"
        "CEAS_PredefCurrent"
        "PanneauPV"

           ];
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir, xpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Passif"

    cos_blocks = [ "MEAB_Resistor"
                   "MEAB_VariableResistor"
                   "MEAB_HeatingResistor"
                   "MEAB_Capacitor"
                   "MEAB_VariableCapacitor"
                   "MEAB_Inductor"
                   "MEAB_VariableInductor"
                   "MEAI_IdealTransformer"
                   "CEAI_IdealDiode"
                   "MEAI_Idle"
                   "MEAI_Short"
                   "MEAI_IdealCommutSwitch"
                   "MEAI_IdealOpeningSwitch"
                   "MEAI_IdealClosingSwitch"
                   "CEAB_EMFGEN"
                   "CEAB_TranslationalEMFGEN"
                 ]
        blocks=[
        "DiodePV"
        "SIMM_EMF_SINUS"
        "PowerTransistor"
           ];
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, [subdir, "Composant basique"], xpal);
    xpal =tbx_build_pal(toolbox_dir, name_subpal, blocks, [subdir, "Composant basique"], xpal);
    xcosPalAdd(xpal, [subdir, "Composant basique"]);

    name_subpal= "Actif"
    cos_blocks = [
        "MEAI_IdealOpAmp3Pin"
        "MEAI_IdealOpAmpLimited"
        "MEAB_VCV"
        "MEAB_VCC"
        "MEAB_CCV"
        "MEAB_CCC"

                 ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, [subdir, "Composant basique"], xpal);
    xcosPalAdd(xpal, [subdir, "Composant basique"]);

    name_subpal= "Passif"

    cos_blocks = [ "MEAS_Diode"
                   "CEAS_ZDiode"
                   "MEAS_HeatingDiode"
                   "MEAB_SaturatingInductor"
                   "MEAB_Transformer"
                   "MEAI_CloserWithArc"
                 ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, [subdir, "Composant avancé"], xpal);
    xcosPalAdd(xpal, [subdir, "Composant avancé"]);

    name_subpal= "Actif"

    cos_blocks = [
        "MEAB_OpAmp"
        "MEAI_IdealOpAmp"
        "MEAS_PMOS"
        "MEAS_NMOS"
        "MEAS_NPN"
        "MEAS_PNP"
        "CEAS_Thyristor"
        "MEAS_HeatingPMOS"
        "MEAS_HeatingNMOS"
        "MEAS_HeatingNPN"
        "MEAS_HeatingPNP"
                 ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, [subdir, "Composant avancé"], xpal);
    xcosPalAdd(xpal, [subdir, "Composant avancé"]);

    name_subpal= "Mesure"

    cos_blocks = [
        "MEAS_PotentialSensor"
        "MEAS_VoltageSensor"
        "MEAS_CurrentSensor"
        "CEAS_PowerSensor"
                 ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

endfunction

runMe();
clear runMe;
