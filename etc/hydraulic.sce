// This file is released under the 3-clause BSD license. See COPYING-BSD.
function runMe()
    toolbox_dir = get_absolute_file_path("hydraulic.sce")+"..";
    subdir=[ 'SIMM', 'Hydraulique']

    name_subpal= _("Sources")
  
    blocks=[
        "Pressure"
        "FlowVolumetric"
           ];
    xpal = xcosPal(name_subpal);

    xpal =tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Composants"
  
    blocks=[
        "CheckValve"
        "SimpleCylinder"
        "Cylinder"
        "Pump"
        "PumpCylinderVariable"
        "Tank"
        "Valve22"
        "Valve33"
        "Valve43"
        "Valve53"
        "PressureForceTranslator"
        "HydraulicCapacity"
        "VolEfficiency"
           ];
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

//    name_subpal= __("Parts")
//  
//    blocks=[
//        "Laminar"
//        "Turbulent"
//        "TurbulentControled"
//        "RotComp"
//        "ChamberLeft"
//        "ChamberRight"
//        "VolEfficiency"
//        "HydraulicCapacity"
//           ];
//    xpal = xcosPal(name_subpal);
//    xpal =tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir, xpal);
//    xcosPalAdd(xpal, subdir);
//

    name_subpal= "Capteurs"
  
    blocks=[
        "PressureSensor"
        "FlowVolumetricSensor"
           ];
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

endfunction

runMe();
clear runMe;
