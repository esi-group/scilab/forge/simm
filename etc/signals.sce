// This file is released under the 3-clause BSD license. See COPYING-BSD.
function runMe()
    toolbox_dir = get_absolute_file_path("signals.sce")+"..";
    subdir=[ 'SIMM', 'Signaux']

    name_subpal= _("Sources")
    cos_blocks = ["MBS_Constant"
                  "MBS_Step"
                  "MBS_Ramp"
                  "MBS_Sine"
                  "CBS_Trapezoid"
                  "MBS_ExpSine"
                  "MBS_Exponentials"
                  "MBS_Pulse"
                  "MBS_SawTooth"
                  "MBS_Clock"
                  "CCP_PWM"
                 ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Continu"
    cos_blocks = [   "MBC_Integrator"
                     "MBC_Der"
                     "MBC_FirstOrder"
                     "MBC_SecondOrder"
                     "CBC_TransferFunction"
                     "MBC_PI"
                     "MBC_Derivative"
                     "MBC_PID"
                 ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Non-linéarités"
    cos_blocks = [    "CBN_Hysteresis"
                      "CBN_RateLimiter"
                      "MBN_DeadZone"
                      "MBN_Limiter"
                 ]
     blocks=[
        "SIMM_DELAY"
           ];
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xpal =tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= _("Math")
    cos_blocks = [   "MBM_Abs"
                     "MBM_Acos"
                     "MBM_Add"
                     "CBM_Add3"
                     "MBM_Asin"
                     "MBM_Atan"
                     "MBM_Cos"
                     "MBM_Cosh"
                     "MBM_Division"
                     "MBM_Exp"
                     "MBM_Feedback"
                     "MBM_Gain"
                     "MBM_Log"
                     "MBM_Log10"
                     "MBM_Max"
                     "MBM_Min"
                     "MBM_Product"
                     "MBM_Sign"
                     "MBM_Sin"
                     "MBM_Sinh"
                     "MBM_Sqrt"
                     "MBM_Tan"
                     "MBM_Tanh"
                     "CBM_Atan2"
                     "CBM_Sum"
                     "CBM_TwoInputs"
                     "CBM_TwoOutputs"
                     "CBMV_Add"
                     "CBMV_CrossProduct"
                     "CBMV_DotProduct"
                     "CBMV_ElementwiseProduct"
                 ]

    blocks=[
        "SIMM_POSITIV"
        "SIMM_LOGICAL_OP"
        "SIMM_RELATIONALOP"
           ];
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xpal =tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

endfunction

runMe();
clear runMe;
