// This file is released under the 3-clause BSD license. See COPYING-BSD.
function runMe()
    toolbox_dir = get_absolute_file_path("mechanics_rotational.sce")+"..";
    subdir=[ 'SIMM', 'Mecanique','Rotation 1D']

    name_subpal= "Sources"
    cos_blocks = [
                  "CMRS_ImposedKinematic"
                  "CMRS_Torque0"
                  "CMRS_Torque"
                  "CMRS_Torque2"
//                  "CMRS_ConstantTorque"
//                  "CMRS_ConstantSpeed"
//                  "CMRS_TorqueStep"
//                  "CMRS_LinearSpeedDependen"
//                  "CMRS_QuadraticSpeedDepen"
                  ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Basique"
    cos_blocks = [
                    "MMR_Fixed"
                    "CMRC_Free"
                    "MMR_Inertia"
                    "CMRC_Disc"
                    "MMR_Spring"
                    "MMR_Damper"
                    "MMR_SpringDamper"
                    "CMRC_ElastoBacklash"
                    "CMR_BearingFriction"
                     ]
    blocks = [
                    "RotationalFriction"
                    ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xpal =tbx_build_pal(toolbox_dir, name_subpal, blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Mesure"
    cos_blocks = [
                  "MMRS_TorqueSensor"
                  "CMRS_PowerSensor"
                  "CMRS_GenSensor"
                  "CMRS_GenRelSensor"
                     ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);
endfunction

runMe();
clear runMe;
