// This file is released under the 3-clause BSD license. See COPYING-BSD.
function runMe()
    toolbox_dir = get_absolute_file_path("mechanics_planar.sce")+"..";
    subdir=['SIMM', 'Mecanique','Plane']

    name_subpal= "Liaisons"
    cos_blocks = [
                 "CMPJ_FreeMotion"
                     "CMPJ_Prismatic"
                     "CMPJ_Revolute"
                     "CMPJ_ActuatedRevolute"
                     "CMPJ_RollingWheel"
                     "CMPJ_ActuatedRollingWhee"
                     "CMPJ_ActuatedPrismatic"
                     "CMPL_Prismatic"
                    "CMPL_Revolute"
                    "CMPL_ActuatedPrismatic"
                    "CMPL_ActuatedRevolute"
                  ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Actions mécaniques"
    cos_blocks = [
                 "CMP_World"
                  "CMPF_WorldForce"
                     "CMPF_WorldTorque"
                     "CMPF_FrameForce"
                     "CMPF_LineForce"
                     "CMPF_LineForceWithMass"
                  ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Solides"
    cos_blocks = [
                    "CMPP_Fixed"
                    "CMPP_FixedTranslation"
                    "CMPP_FixedRotation"
                    "CMPP_Body"
                    "CMPP_BodyShape"
                    "CMPP_PointMass"
                     ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);

    name_subpal= "Mesure"
    cos_blocks = [
                "CMPS_AbsPosition"
                    "CMPS_AbsVelocity"
                    "CMPS_AbsAcceleration"
                    "CMPS_GenSensor"
                    "CMPS_AbsAngle"
                    "CMPS_AbsAngularVelocity"
                    "CMPS_AbsAngularAccelerat"
                    "CMPS_AbsPosition2"
                    "CMPS_AbsVelocity2"
                    "CMPS_AbsAcceleration2"
                  "CMPS_Distance"
                    "CMPS_CutForce"
                    "CMPS_CutForce2"
                    "CMPS_CutTorque"
                    "CMPS_Power"
                    "CMPS_RelPosition"
                    "CMPS_RelVelocity"
                    "CMPS_RelAcceleration"
                    "CMPS_GenRelSensor"
                    "CMPS_Angle"
                    "CMPS_RelAngularVelocity"
                    "CMPS_RelAngularAccelerat"
                    "CMPS_RelPosition2"
                    "CMPS_RelVelocity2"
                    "CMPS_RelAcceleration2"
                     ]
    xpal = xcosPal(name_subpal);
    xpal =tbx_build_pal_from_cos(toolbox_dir, name_subpal, cos_blocks, subdir, xpal);
    xcosPalAdd(xpal, subdir);
endfunction

runMe();
clear runMe;
